SOURCES += \
        $$PWD/src/charsmorewidget.cpp \
        $$PWD/src/charswidget.cpp \
        $$PWD/src/dragwidget.cpp \
        $$PWD/src/kbbutton.cpp \
        $$PWD/src/kbtitle.cpp \
        $$PWD/src/letterswidget.cpp \
        $$PWD/src/numberswidget.cpp \
        $$PWD/src/virtualkeyboardwidget.cpp \
        $$PWD/src/x11keyboard.cpp


HEADERS += \
        $$PWD/src/charsmorewidget.h \
        $$PWD/src/charswidget.h \
        $$PWD/src/commondef.h \
        $$PWD/src/dragwidget.h \
        $$PWD/src/kbbutton.h \
        $$PWD/src/kbtitle.h \
        $$PWD/src/letterswidget.h \
        $$PWD/src/numberswidget.h \
        $$PWD/src/virtualkeyboardwidget.h \
        $$PWD/src/x11keyboard.h


RESOURCES += \
        $$PWD/src/keyboard.qrc
