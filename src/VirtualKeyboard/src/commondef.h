/*
 * Copyright (C) 2023 KylinSoftCo., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
**/
#ifndef COMMONDEF_H
#define COMMONDEF_H

#define KEYBOARD_PARENT_DEFAULT_WIDTH 1620
#define KEYBOARD_PARENT_DEFAULT_HEIGHT 1080

#define KEYBOARD_FIXED_DEFAULT_WIDTH  1620
#define KEYBOARD_DRAGSHOW_FIXED_DEFAULT_HEIGHT 428

#define KEYBOARD_DRAGSHOW_FIXED_DEFAULT_WIDTH 1458
#define KEYBOARD_DRAGHIDE_FIXED_DEFAULT_HEIGHT 404

#define KEYBOARD_TITLEBTN_DEFAULT_WIDTH 56
#define KEYBOARD_TITLEBTN_DEFAULT_HEIGHT 56
#define KEYBOARD_TITLE_DEFAULT_HEIGHT 68

#define KEYBOARD_DRAGBTN_DEFAULT_WIDTH 56
#define KEYBOARD_DRAGBTN_DEFAULT_HEIGHT 4
#define KEYBOARD_DRAG_DEFAULT_HEIGHT 26

#define KEYBOARD_FLOAT_PERCENTAGE 0.9

#define KEYBOARD_FIXED_DEFAULT_LMARGIN 22   // 左边距
#define KEYBOARD_FIXED_DEFAULT_RMARGIN 22    // 右边距
#define KEYBOARD_FIXED_DEFAULT_TMARGIN 8    // 上边距
#define KEYBOARD_FIXED_DEFAULT_BMARGIN 16    // 下边距

#define KEYBOAED_FIXED_DEFAULT_VSPACING 8   // 垂直间隔
#define KEYBOAED_FIXED_DEFAULT_HSPACING 8   // 水平间隔
// letters
#define KEYBOARD_FIXED_DEFAULT_LETTER_L1 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)     // 字符页面左起点
#define KEYBOARD_FIXED_DEFAULT_LETTER_L2 (KEYBOARD_FIXED_DEFAULT_LMARGIN+72)    // 字符页面左起点
#define KEYBOARD_FIXED_DEFAULT_LETTER_L3 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)     // 字符页面左起点
#define KEYBOARD_FIXED_DEFAULT_LETTER_L4 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)     // 字符页面左起点
#define KEYBOARD_FIXED_DEFAULT_LETTER_WIDTH 136 // 字符按钮宽度
#define KEYBOARD_FIXED_DEFAULT_LETTER_HEIGHT 72 // 字符按钮高度
#define KEYBOARD_FIXED_DEFAULT_LETTER_ENTWIDTH 208
#define KEYBOARD_FIXED_DEFAULT_LETTER_SPCWIDTH 712
#define KEYBOARD_FIXED_DEFAULT_TITLEBTN_WIDTH 56
#define KEYBOARD_FIXED_DEFAULT_TITLEBTN_HEIGHT 56

#define KEYBOARD_FIXED_DEFAULT_ENTERBTN_WIDTH 208
#define KEYBOARD_FIXED_DEFAULT_SPACEBTN_WIDTH 712

#define KEYBOARD_LETTER_COLOR_NORMAL "#FFFFFF"
#define KEYBOARD_LETTER_COLOR_PRESSED "#DDE0E4"
#define KEYBOARD_LETTER_COLOR_BORDER_NORMAL "#95A0AD"
#define KEYBOARD_LETTER_COLOR_BORDER_PRESSED "#95A0AD"

#define KEYBOARD_OTHER_COLOR_NORMAL "#CED3D9"
#define KEYBOARD_OTHER_COLOR_PRESSED "#3790FA"
#define KEYBOARD_OTHER_COLOR_BORDER_NORMAL "#95A0AD"
#define KEYBOARD_OTHER_COLOR_BORDER_PRESSED "#1174E5"

#define KEYBOARD_FONT_COLOR_PRESS "#262626"
#define KEYBOARD_OTHER_FONT_COLOR_PRESS "#FFFFFF"

//numbers
#define KEYBOARD_FIXED_DEFAULT_NUMBER_L1 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)
#define KEYBOARD_FIXED_DEFAULT_NUMBER_L2 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)
#define KEYBOARD_FIXED_DEFAULT_NUMBER_L3 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)
#define KEYBOARD_FIXED_DEFAULT_NUMBER_L4 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)

#define KEYBOARD_FIXED_DEFAULT_NUMBER_WIDTH 306 // 数字按钮宽度
#define KEYBOARD_FIXED_DEFAULT_NUMBER_HEIGHT 72 // 数字按钮高度
#define KEYBOARD_FIXED_DEFAULT_NUMBER_CHARS_HEIGHT 230 // 数字字符按钮高度

// chars more

#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_L1 (KEYBOARD_FIXED_DEFAULT_LMARGIN+0)
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_HSPACING 16
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_VSPACING 16

#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_LIST_COLS  6
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_LIST_WIDTH 1350 // 字符列表宽度
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_LIST_HEIGHT 315 // 字符列表高度
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_BTN_WIDTH 214 // 按钮宽度
#define KEYBOARD_FIXED_DEFAULT_CHARSMORE_BTN_HEIGHT 94 // 功能按钮高度
#define KEYBOARD_FIXED_DEFAULT_NORMAL_CHARSMORE_BTN_HEIGHT 80 // 符号按钮高度

//iconsize
#define KEYBOARD_FIXED_DEFAULT_ICONSIZE QSize(25, 25)

//keyName
#define PAGE_CHAR "page_char"
#define PAGE_NUMBER "page_number"
#define PAGE_CHARSMORE "page_charmore"
#define PAGE_LETTER "page_letter"
#define BTN_FLOAT "float"
#define BTN_CLOSE "close"
#define BTN_RETURN "return"
#define BTN_BACK "backspace"
#define BTN_ENTER "enter"
#define BTN_SHIFT "shift"
#define BTN_CTRL "ctrl"
#define BTN_ALT "alt"
#define BTN_SPACE "space"
#define BTN_RIGHT "right"
#define BTN_LEFT "left"
#define BTN_CAPSLOCK "capslock"



#endif // COMMONDEF_H
