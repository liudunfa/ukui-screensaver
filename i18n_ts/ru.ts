<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AgreementWindow</name>
    <message>
        <source>I know</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">Дополнительные устройства</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">Биометрические</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">отпереть</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">LoggedIn</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">Пароль неверен, повторите попытку</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="unfinished">FingerPrint</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="unfinished">FingerVein</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="unfinished">Ирис</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="unfinished">Лицо</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="unfinished">Voiceprint</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="unfinished">гость</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify face recognition or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press fingerprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify finger vein or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify iris or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User name input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authentication failure, Please try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please try again in %1 minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please try again in %1 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account locked permanently.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abnormal network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Face recognition waiting time out, please click refresh or enter the password to unlock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fingervein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>voiceprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BatteryWidget</name>
    <message>
        <source>Charging...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fully charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PowerMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BatteryMode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BioAuthWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">TextLabel</translation>
    </message>
    <message>
        <source>More</source>
        <translation type="obsolete">Больше</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="obsolete">Retry</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">FingerPrint</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">FingerVein</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">Ирис</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">Лицо</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">Voiceprint</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">Выберите другие биометрические устройства</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">Тип устройства:</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">Имя устройства:</translation>
    </message>
</context>
<context>
    <name>BlockWidget</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Confrim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following programs prevent restarting, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following programs prevent the shutdown, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following programs prevent suspend, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following programs prevent hibernation, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following programs prevent you from logging out, you can click &quot;Cancel&quot; and then close them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you do not perform any operation, the system will automatically %1 after %2 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>shut down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <source>More</source>
        <translation type="unfinished">Больше</translation>
    </message>
    <message>
        <source>ABC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">FingerPrint</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">FingerVein</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">Ирис</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">Лицо</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">Voiceprint</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">пароль</translation>
    </message>
</context>
<context>
    <name>EngineDevice</name>
    <message>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1% available, charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left %1h %2m (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1% available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left %1h %2m to full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>charging (%1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 waiting to discharge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 waiting to charge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AC adapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Laptop battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>UPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Media player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tablet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unrecognised</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KBTitle</name>
    <message>
        <source>Suspended state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Welt status</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <source>KeyboardWidget</source>
        <translation type="vanished">KeyboardWidget</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightDMHelper</name>
    <message>
        <source>Guest</source>
        <translation type="unfinished">гость</translation>
    </message>
    <message>
        <source>failed to start session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">форма</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Дата</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Время</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">гость</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation>Сменить пользователя</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to %1 this system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>system-monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 may cause users who have logged in to this computer to lose content that has not yet been stored,To still perform please click &quot;Confirm&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SwitchSession</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Power Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>VirtualKeyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <source>Password</source>
        <translation type="unfinished">пароль</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="unfinished">FingerPrint</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="unfinished">FingerVein</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="unfinished">Ирис</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="unfinished">Лицо</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="unfinished">Voiceprint</translation>
    </message>
    <message>
        <source>Login Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Identify device removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPRISWidget</name>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyNetworkWidget</name>
    <message>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
</context>
<context>
    <name>PowerListWidget</name>
    <message>
        <source>Hibernate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close all apps, and then shut down your computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close all apps, and then restart your computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user logs out of the system, terminates the session, and returns to the login page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="unfinished">Сменить пользователя</translation>
    </message>
    <message>
        <source>LockScreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>UpgradeThenRestart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>UpgradeThenShutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The computer stays on, but consumes less power, The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <source>SwitchUser</source>
        <translation type="obsolete">Сменить пользователя</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="unfinished">FingerPrint</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="unfinished">FingerVein</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="unfinished">Ирис</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="unfinished">Лицо</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="unfinished">Voiceprint</translation>
    </message>
    <message>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The screensaver is active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The screensaver is inactive.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>Picture does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <source>You have rested</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">форма</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">TextLabel</translation>
    </message>
</context>
<context>
    <name>UserListWidget</name>
    <message>
        <source>Guest</source>
        <translation type="unfinished">гость</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LoadPlugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>action</name>
    <message>
        <source>which block type,param:Suspend/Hibernate/Restart/Shutdown/Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>which block type,param:Shutdown/Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>which block type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <source>how long to show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <source>if show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>Начните команду для ukui ScreenSaver.</translation>
    </message>
    <message>
        <source>lock the screen immediately</source>
        <translation>немедленно заблокируйте экран</translation>
    </message>
    <message>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>Диалог для экранного экрана ukui.</translation>
    </message>
    <message>
        <source>activated by session idle signal</source>
        <translation>активируется сигналом холостого хода</translation>
    </message>
    <message>
        <source>Backend for the ukui ScreenSaver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>lock the screen by startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>query the status of the screen saver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unlock the screen saver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show the screensaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>lock the screen and show screensaver immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show screensaver immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show blank screensaver immediately and if lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screensaver for ukui-screensaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show on root window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show on window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>window id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show the session tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show the switchuser window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show the app block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show the multiUsers block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show switch user window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
