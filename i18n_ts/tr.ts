<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../src/widgets/agreementwindow.cpp" line="47"/>
        <source>I know</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">Diğer Cihazlar</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">Biometrik</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1173"/>
        <source>Retry</source>
        <translation type="unfinished">Yeniden Dene</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">Kilidi Aç</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">%2 başarısız denemeden dolayı hesap %1 dakika kilitlendi</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">Parola yanlış, Lütfen tekrar deneyin</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">Kimlik doğrulama hatası, hala %1 kalan denemen var</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="456"/>
        <location filename="../src/widgets/authdialog.cpp" line="1165"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="458"/>
        <source>Guest</source>
        <translation type="unfinished">Misafir</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="652"/>
        <location filename="../src/widgets/authdialog.cpp" line="680"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="655"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="658"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="661"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="664"/>
        <source>Verify iris or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="667"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="791"/>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="934"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="937"/>
        <source>Input Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="942"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1012"/>
        <source>User name input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1015"/>
        <location filename="../src/widgets/authdialog.cpp" line="1019"/>
        <source>Authentication failure, Please try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1091"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1244"/>
        <source>Please try again in %1 minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1251"/>
        <source>Please try again in %1 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1258"/>
        <source>Account locked permanently.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1477"/>
        <location filename="../src/widgets/authdialog.cpp" line="1627"/>
        <location filename="../src/widgets/authdialog.cpp" line="1771"/>
        <location filename="../src/widgets/authdialog.cpp" line="1952"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1480"/>
        <location filename="../src/widgets/authdialog.cpp" line="1630"/>
        <location filename="../src/widgets/authdialog.cpp" line="1776"/>
        <location filename="../src/widgets/authdialog.cpp" line="1779"/>
        <location filename="../src/widgets/authdialog.cpp" line="1955"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1793"/>
        <location filename="../src/widgets/authdialog.cpp" line="1797"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1808"/>
        <source>Abnormal network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1816"/>
        <source>Face recognition waiting time out, please click refresh or enter the password to unlock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2079"/>
        <source>FingerPrint</source>
        <translation type="unfinished">Parmak İzi</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2081"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2083"/>
        <source>Iris</source>
        <translation type="unfinished">Göz</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2085"/>
        <source>Face</source>
        <translation type="unfinished">Yüz</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2087"/>
        <source>VoicePrint</source>
        <translation type="unfinished">Ses İzi</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2089"/>
        <location filename="../src/widgets/authdialog.cpp" line="2111"/>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2091"/>
        <location filename="../src/widgets/authdialog.cpp" line="2113"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2101"/>
        <source>fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2103"/>
        <source>fingervein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2105"/>
        <source>iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2107"/>
        <source>face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2109"/>
        <source>voiceprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="obsolete">Biyometrik Kimlik Doğrulama</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="obsolete">Parola Doğrulama</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="obsolete">Diğer Ayıtlar</translation>
    </message>
</context>
<context>
    <name>BatteryWidget</name>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="112"/>
        <location filename="../src/widgets/batterywidget.cpp" line="142"/>
        <source>Charging...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="114"/>
        <location filename="../src/widgets/batterywidget.cpp" line="144"/>
        <source>fully charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="118"/>
        <location filename="../src/widgets/batterywidget.cpp" line="148"/>
        <source>PowerMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="121"/>
        <location filename="../src/widgets/batterywidget.cpp" line="151"/>
        <source>BatteryMode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">Parmak İzi</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">Damar İzi</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">Göz</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">Yüz</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">Ses İzi</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">Lütfen diğer biyometrik cihazları seçin</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">Aygıt Türü:</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">Aygıt Adı:</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <source>Current device: </source>
        <translation type="obsolete">Şuanki aygıt:</translation>
    </message>
    <message>
        <source>Identify failed, Please retry.</source>
        <translation type="obsolete">Tanımlama başarısız, Lütfen tekrar deneyin.</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <source>Please select the biometric device</source>
        <translation type="obsolete">Lütfen biyometrik aygıtı seçin</translation>
    </message>
    <message>
        <source>Device type:</source>
        <translation type="obsolete">Aygıt türü:</translation>
    </message>
    <message>
        <source>Device name:</source>
        <translation type="obsolete">Aygıt adı:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>BlockWidget</name>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="60"/>
        <location filename="../src/widgets/blockwidget.cpp" line="140"/>
        <location filename="../src/widgets/blockwidget.cpp" line="239"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="64"/>
        <location filename="../src/widgets/blockwidget.cpp" line="141"/>
        <location filename="../src/widgets/blockwidget.cpp" line="240"/>
        <source>Confrim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="147"/>
        <location filename="../src/widgets/blockwidget.cpp" line="166"/>
        <source>If you do not perform any operation, the system will automatically %1 after %2 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="181"/>
        <source>The following programs prevent restarting, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="185"/>
        <source>The following programs prevent the shutdown, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="190"/>
        <source>The following programs prevent suspend, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="193"/>
        <source>The following programs prevent hibernation, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="197"/>
        <source>The following programs prevent you from logging out, you can click &quot;Cancel&quot; and then close them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="247"/>
        <source>shut down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="250"/>
        <source>restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/charsmorewidget.cpp" line="183"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="115"/>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="273"/>
        <source>More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="129"/>
        <source>ABC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="142"/>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">Parmak İzi</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">Parmak Damarı</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">Göz</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">Yüz</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">Ses İzi</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>EngineDevice</name>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="284"/>
        <source>%1% available, charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="290"/>
        <source>Left %1h %2m (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="295"/>
        <source>%1% available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="301"/>
        <source>Left %1h %2m to full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="303"/>
        <source>charging (%1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="309"/>
        <source>%1 waiting to discharge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="314"/>
        <source>%1 waiting to charge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="334"/>
        <source>AC adapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="338"/>
        <source>Laptop battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="342"/>
        <source>UPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="346"/>
        <source>Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="350"/>
        <source>Mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="354"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="358"/>
        <source>PDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="362"/>
        <source>Cell phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="366"/>
        <source>Media player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="370"/>
        <source>Tablet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="374"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="378"/>
        <source>unrecognised</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../src/widgets/iconedit.cpp" line="92"/>
        <source>OK</source>
        <translation type="unfinished">Tamam</translation>
    </message>
</context>
<context>
    <name>KBTitle</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="46"/>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="83"/>
        <source>Suspended state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="57"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="86"/>
        <source>Welt status</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="172"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="186"/>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightDMHelper</name>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="121"/>
        <source>failed to start session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="215"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="224"/>
        <source>Guest</source>
        <translation type="unfinished">Misafir</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <source>Date</source>
        <translation type="obsolete">Tarih</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zaman</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="obsolete">Misafir</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="514"/>
        <source>SwitchUser</source>
        <translation>Kullanıcı Değiştir</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="544"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="568"/>
        <source>VirtualKeyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="581"/>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="625"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1727"/>
        <source>system-monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1253"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1259"/>
        <source>%1 may cause users who have logged in to this computer to lose content that has not yet been stored,To still perform please click &quot;Confirm&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1289"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1294"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to %1 this system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1255"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1290"/>
        <source>Restart</source>
        <translation type="unfinished">Yeniden Başlat</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="483"/>
        <source>SwitchSession</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="501"/>
        <source>Power Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1261"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1295"/>
        <source>Shut Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="82"/>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="105"/>
        <source>Login Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="289"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="315"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="703"/>
        <source>Identify device removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1034"/>
        <source>FingerPrint</source>
        <translation type="unfinished">Parmak İzi</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1036"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1038"/>
        <source>Iris</source>
        <translation type="unfinished">Göz</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1040"/>
        <source>Face</source>
        <translation type="unfinished">Yüz</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1042"/>
        <source>VoicePrint</source>
        <translation type="unfinished">Ses İzi</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1044"/>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1046"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPRISWidget</name>
    <message>
        <location filename="../src/widgets/mpriswidget.cpp" line="214"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="215"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="228"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="237"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyNetworkWidget</name>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="52"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="54"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="161"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="175"/>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="310"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerListWidget</name>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="299"/>
        <location filename="../src/widgets/powerlistwidget.h" line="109"/>
        <source>Hibernate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="300"/>
        <location filename="../src/widgets/powerlistwidget.h" line="110"/>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="303"/>
        <location filename="../src/widgets/powerlistwidget.h" line="122"/>
        <source>Suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="304"/>
        <location filename="../src/widgets/powerlistwidget.h" line="123"/>
        <source>The computer stays on, but consumes less power, The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="307"/>
        <location filename="../src/widgets/powerlistwidget.h" line="172"/>
        <source>Restart</source>
        <translation type="unfinished">Yeniden Başlat</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="308"/>
        <source>Close all apps, and then restart your computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="310"/>
        <location filename="../src/widgets/powerlistwidget.h" line="196"/>
        <source>Shut Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="311"/>
        <location filename="../src/widgets/powerlistwidget.h" line="197"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="313"/>
        <location filename="../src/widgets/powerlistwidget.h" line="147"/>
        <source>Log Out</source>
        <translation type="unfinished">Çıkış</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="315"/>
        <location filename="../src/widgets/powerlistwidget.h" line="149"/>
        <source>The current user logs out of the system, terminates the session, and returns to the login page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="317"/>
        <location filename="../src/widgets/powerlistwidget.h" line="97"/>
        <source>SwitchUser</source>
        <translation type="unfinished">Kullanıcı Değiştir</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="319"/>
        <location filename="../src/widgets/powerlistwidget.h" line="135"/>
        <source>LockScreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="321"/>
        <location filename="../src/widgets/powerlistwidget.h" line="160"/>
        <source>UpgradeThenRestart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="323"/>
        <location filename="../src/widgets/powerlistwidget.h" line="184"/>
        <source>UpgradeThenShutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="325"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="obsolete">Bilgisayarı Kapat</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.h" line="173"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <source>lock</source>
        <translation type="vanished">kilit</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">Kullanıcı Değiştir</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">Çıkış</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">yeniden başlat</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">kapat</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="obsolete">Ekranı Kilitle</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="obsolete">Kullanıcı Değiştir</translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation type="obsolete">Çıkış</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">Yeniden Başlat</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="obsolete">Bilgisayarı Kapat</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/common/biodefines.cpp" line="28"/>
        <source>FingerPrint</source>
        <translation type="unfinished">Parmak İzi</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="30"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="32"/>
        <source>Iris</source>
        <translation type="unfinished">Göz</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="34"/>
        <source>Face</source>
        <translation type="unfinished">Yüz</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="36"/>
        <source>VoicePrint</source>
        <translation type="unfinished">Ses İzi</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="38"/>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="40"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="108"/>
        <source>The screensaver is active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="110"/>
        <source>The screensaver is inactive.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="../src/enginedevice.cpp" line="308"/>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">çıkış(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="obsolete">çıkış</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="obsolete">Masaüstü arkaplanı olarak ayarla</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="obsolete">Otomatik değiştir</translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="136"/>
        <source>Picture does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="139"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <location filename="../src/screensaver/sleeptime.cpp" line="75"/>
        <source>You have rested</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserListWidget</name>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="67"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="121"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="69"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="123"/>
        <source>Guest</source>
        <translation type="unfinished">Misafir</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="14"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="26"/>
        <source>LoadPlugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>action</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="69"/>
        <source>which block type,param:Suspend/Hibernate/Restart/Shutdown/Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="74"/>
        <source>which block type,param:Shutdown/Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="76"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="81"/>
        <source>which block type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="62"/>
        <source>how long to show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="67"/>
        <source>if show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="44"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation type="unfinished">Ukui Ekran Koruyucu için başlatma komutu.</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="49"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="46"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="48"/>
        <source>lock the screen immediately</source>
        <translation type="unfinished">Ekranı hemen kilitle</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="52"/>
        <source>query the status of the screen saver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="55"/>
        <source>unlock the screen saver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="57"/>
        <source>show the screensaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="60"/>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="63"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="70"/>
        <source>show the session tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="65"/>
        <source>show the switchuser window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="68"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="75"/>
        <source>show the app block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="73"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="80"/>
        <source>show the multiUsers block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="56"/>
        <source>Backend for the ukui ScreenSaver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="62"/>
        <source>lock the screen by startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="40"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="50"/>
        <source>activated by session idle signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="53"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="58"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="55"/>
        <source>show screensaver immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="61"/>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="66"/>
        <source>show blank screensaver immediately and if lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="72"/>
        <source>show switch user window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="63"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="67"/>
        <source>show on root window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="69"/>
        <source>show on window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="70"/>
        <source>window id</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
