<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../src/widgets/agreementwindow.cpp" line="47"/>
        <source>I know</source>
        <translation>ངས་ཤེས་སོང་།</translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">选择其他设备</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">使用生物识别认证</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">使用密码认证</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1173"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">解锁</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">已登录</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1244"/>
        <source>Please try again in %1 minutes.</source>
        <translation>ཁྱོད་ཀྱིས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1251"/>
        <source>Please try again in %1 seconds.</source>
        <translation>ཁྱོད་ཀྱིས་དུས་ཚོད་སྐར་ཆ་གཅིག་གི་ནང་དུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1258"/>
        <source>Account locked permanently.</source>
        <translation>དུས་གཏན་དུ་ཟྭ་བརྒྱབ་པའི་རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="652"/>
        <location filename="../src/widgets/authdialog.cpp" line="680"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>ངོ་གདོང་ངོས་འཛིན་ལ་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་ནང་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="458"/>
        <source>Guest</source>
        <translation type="unfinished">游客</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="655"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>མཛུབ་རིས་མནན་པའམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="658"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>སྒྲ་པར་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="661"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>མཛུབ་མོའི་ནང་དུ་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="664"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>iris ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="934"/>
        <source>Password:</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="937"/>
        <source>Input Password</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="942"/>
        <source>Username</source>
        <translation type="unfinished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1012"/>
        <source>User name input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1091"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1477"/>
        <location filename="../src/widgets/authdialog.cpp" line="1627"/>
        <location filename="../src/widgets/authdialog.cpp" line="1771"/>
        <location filename="../src/widgets/authdialog.cpp" line="1952"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་མ་ཐུབ་ན། གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1480"/>
        <location filename="../src/widgets/authdialog.cpp" line="1630"/>
        <location filename="../src/widgets/authdialog.cpp" line="1776"/>
        <location filename="../src/widgets/authdialog.cpp" line="1779"/>
        <location filename="../src/widgets/authdialog.cpp" line="1955"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་ཐབས་བྲལ་བ་དང་། གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1808"/>
        <source>Abnormal network</source>
        <translation>རྒྱུན་ལྡན་མིན་པའི་དྲ་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1816"/>
        <source>Face recognition waiting time out, please click refresh or enter the password to unlock.</source>
        <translation>མིའི་གདོང་ལ་དབྱེ་འབྱེད་བྱེད་པར་རེ་སྒུག་བྱེད་དུས་གསང་གྲངས་ཀྱི་སྒོ་ལྕགས་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2079"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2081"/>
        <source>FingerVein</source>
        <translation>FingerVein</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2083"/>
        <source>Iris</source>
        <translation>Iris</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2085"/>
        <source>Face</source>
        <translation>མིའི་གདོང་།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2087"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་རིས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2089"/>
        <location filename="../src/widgets/authdialog.cpp" line="2111"/>
        <source>Ukey</source>
        <translation>བདེ་འཇགས་གསང་ལྡེ་</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2091"/>
        <location filename="../src/widgets/authdialog.cpp" line="2113"/>
        <source>QRCode</source>
        <translation>དོན་ཚན་གཉིས་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2101"/>
        <source>fingerprint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2103"/>
        <source>fingervein</source>
        <translation>སྡོད་རྩ་ཞེས་པ་ནི་སྡོད་རྩར</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2105"/>
        <source>iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2107"/>
        <source>face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2109"/>
        <source>voiceprint</source>
        <translation>སྒྲ་གདངས་ཀྱི་རི་མོ།</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="791"/>
        <source>Password cannot be empty</source>
        <translation>གསང་གྲངས་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1793"/>
        <location filename="../src/widgets/authdialog.cpp" line="1797"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་མ་ཐུབ་ན། ཁྱེད་ཚོར་ད་དུང་%2ལ་ཞིབ་བཤེར་བྱེད་པའི་གོ་སྐབས་ཡོད།</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">密码错误，请重试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩%1次尝试机会</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1015"/>
        <location filename="../src/widgets/authdialog.cpp" line="1019"/>
        <source>Authentication failure, Please try again</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་ན། ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="667"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་འཕྲིན་ཕྲན་ཞིབ་བཤེར་ཨང་གྲངས་སམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Enter the ukey password</source>
        <translation type="vanished">གསང་བའི་ཨང་གྲངས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Insert the ukey into the USB port</source>
        <translation type="vanished">བདེ་འཇགས་ཀྱི་གསང་བའི་ལྡེ་མིག་དེ་USBཡི་སྣེ་འདྲེན་དུ་འཇུག་རོགས།</translation>
    </message>
    <message>
        <source>Password </source>
        <translation type="vanished">གསང་གྲངས། </translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="456"/>
        <location filename="../src/widgets/authdialog.cpp" line="1165"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>BatteryWidget</name>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="112"/>
        <location filename="../src/widgets/batterywidget.cpp" line="142"/>
        <source>Charging...</source>
        <translation>ད་ལྟ་གློག་གསོག་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="114"/>
        <location filename="../src/widgets/batterywidget.cpp" line="144"/>
        <source>fully charged</source>
        <translation>དེ་དག་གིས་ཁེངས་འདུག</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="118"/>
        <location filename="../src/widgets/batterywidget.cpp" line="148"/>
        <source>PowerMode</source>
        <translation>གློག་ཁུངས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="121"/>
        <location filename="../src/widgets/batterywidget.cpp" line="151"/>
        <source>BatteryMode</source>
        <translation>གློག་སྨན་གྱི་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">请选择其他生物识别设备</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">设备名称：</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <source>Current device: </source>
        <translation type="vanished">མིག་སྔའི་སྒྲིག་ཆས་ནི། </translation>
    </message>
    <message>
        <source>Identify failed, Please retry.</source>
        <translation type="vanished">ཕམ་ཉེས་བྱུང་བར་ངོས་འཛིན་གནང་རོགས། ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <source>Please select the biometric device</source>
        <translation type="vanished">སྐྱེ་དངོས་དབྱེ་འབྱེད་སྒྲིག་ཆས་འདེམས་རོགས།</translation>
    </message>
    <message>
        <source>Device type:</source>
        <translation type="vanished">སྒྲིག་ཆས་ཀྱི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <source>Device name:</source>
        <translation type="vanished">སྒྲིག་ཆས་ཀྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>BlockWidget</name>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="60"/>
        <location filename="../src/widgets/blockwidget.cpp" line="140"/>
        <location filename="../src/widgets/blockwidget.cpp" line="239"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="64"/>
        <location filename="../src/widgets/blockwidget.cpp" line="141"/>
        <location filename="../src/widgets/blockwidget.cpp" line="240"/>
        <source>Confrim</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="147"/>
        <location filename="../src/widgets/blockwidget.cpp" line="166"/>
        <source>If you do not perform any operation, the system will automatically %1 after %2 seconds.</source>
        <translation>གལ་ཏེ་ཁྱེད་ཀྱིས་བཀོལ་སྤྱད་གང་ཡང་མ་བྱས་ན། མ་ལག་ནི་%2ཡི་རྗེས་སུ་རང་འགུལ་%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="181"/>
        <source>The following programs prevent restarting, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>གཤམ་གྱི་གོ་རིམ་གྱིས་བསྐྱར་དུ་སྒོ་འབྱེད་པར་བཀག་འགོག་བྱས་ན། ཁྱེད་ཀྱིས་&quot;མེད་པར་བཟོ་&quot;བའི་&quot;རྗེས་སུ་གོ་རིམ་འདི་དག་སྒོ་རྒྱག་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="185"/>
        <source>The following programs prevent the shutdown, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>གཤམ་གྱི་གོ་རིམ་གྱིས་འགག་སྒོ་བཀག་འགོག་བྱས་ན་ཁྱེད་ཀྱིས་&quot;མེད་པར་བཟོ་&quot;བའི་རྗེས་སུ་གོ་རིམ་འདི་དག་སྒོ་རྒྱག་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="190"/>
        <source>The following programs prevent suspend, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>གཤམ་གྱི་གོ་རིམ་གྱིས་གཉིད་མི་ཁུག་པར་བཀག་འགོག་བྱས་ན་ཁྱེད་ཀྱིས་&quot;མེད་པར་བཟོ་&quot;བ་དང་དེ་ནས་གོ་རིམ་འདི་དག་སྒོ་རྒྱག་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="193"/>
        <source>The following programs prevent hibernation, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>གཤམ་གྱི་གོ་རིམ་གྱིས་ངལ་གསོ་བར་བཀག་འགོག་བྱས་ན་ཁྱེད་ཀྱིས་&quot;མེད་པར་བཟོ་&quot;བའི་རྗེས་སུ་གོ་རིམ་འདི་དག་སྒོ་རྒྱག་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="197"/>
        <source>The following programs prevent you from logging out, you can click &quot;Cancel&quot; and then close them.</source>
        <translation>གཤམ་གྱི་གོ་རིམ་གྱིས་ཐོ་ཁོངས་ནས་སུབ་པར་བཀག་འགོག་བྱས་ན། ཁྱེད་ཀྱིས་&quot;མེད་པར་བཟོ་&quot;བའི་&quot;རྗེས་སུ་གོ་རིམ་འདི་དག་སྒོ་རྒྱག་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="247"/>
        <source>shut down</source>
        <translation>ལས་མཚམས་འཇོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="250"/>
        <source>restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/charsmorewidget.cpp" line="183"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="114"/>
        <source></source>
        <translation>དེ་བས་ཀྱང་མང་</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="115"/>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="273"/>
        <source>More</source>
        <translation>དེ་བས་ཀྱང་མང་</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="129"/>
        <source>ABC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="142"/>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <source>edit network</source>
        <translation type="vanished">རྩོམ་སྒྲིག་དྲ་བ།</translation>
    </message>
    <message>
        <source>LAN name: </source>
        <translation type="vanished">LANཡི་མིང་ནི། </translation>
    </message>
    <message>
        <source>Method: </source>
        <translation type="vanished">བྱེད་ཐབས་ནི། </translation>
    </message>
    <message>
        <source>Address: </source>
        <translation type="vanished">སྡོད་གནས་ནི། </translation>
    </message>
    <message>
        <source>Netmask: </source>
        <translation type="vanished">དྲ་རྒྱའི་མ་སི་ཁི་ཡིས་བཤད་རྒྱུར། </translation>
    </message>
    <message>
        <source>Gateway: </source>
        <translation type="vanished">འགག་སྒོ་ནི། </translation>
    </message>
    <message>
        <source>DNS 1: </source>
        <translation type="vanished">DNS 1: </translation>
    </message>
    <message>
        <source>DNS 2: </source>
        <translation type="vanished">DNS 2: </translation>
    </message>
    <message>
        <source>Edit Conn</source>
        <translation type="vanished">རྩོམ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <source>Auto(DHCP)</source>
        <translation type="vanished">རང་འགུལ་(DHCP)</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">ལག་དེབ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
    <message>
        <source>Can not create new wired network for without wired card</source>
        <translation type="vanished">སྐུད་ཡོད་བྱང་བུ་མེད་ན་སྐུད་ཡོད་དྲ་རྒྱ་གསར་པ་གཏོད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>New network already created</source>
        <translation type="vanished">དྲ་རྒྱ་གསར་པ་གསར་སྐྲུན་བྱས་ཟིན་པ།</translation>
    </message>
    <message>
        <source>New network settings already finished</source>
        <translation type="vanished">དྲ་རྒྱའི་སྒྲིག་བཀོད་གསར་པ་ལེགས་འགྲུབ་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <source>Edit Network</source>
        <translation type="vanished">རྩོམ་སྒྲིག་དྲ་བ།</translation>
    </message>
    <message>
        <source>Add Wired Network</source>
        <translation type="vanished">སྐུད་ཡོད་བརྙན་འཕྲིན་དྲ་བ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">已创建新的有线网络</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">新的设置已经生效</translation>
    </message>
    <message>
        <source>New settings already effective</source>
        <translation type="vanished">སྒྲིག་གཞི་གསར་པར་ཕན་ནུས་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <source>There is a same named LAN exsits.</source>
        <translation type="obsolete">已有同名连接存在</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">མཛུབ་མོའི་པར་གཞི།</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">མཛུབ་མོ་ཝེ་ལིན།</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">དབྱི་ལི་སི།</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">ངོ་གདོང་།</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <source>Ukey</source>
        <translation type="vanished">བདེ་འཇགས་གསང་ལྡེ་</translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="vanished">དོན་ཚན་གཉིས་པ།</translation>
    </message>
</context>
<context>
    <name>DigitalAuthDialog</name>
    <message>
        <source>LoginByUEdu</source>
        <translation type="vanished">LoginByUEdu</translation>
    </message>
    <message>
        <source>now is authing, wait a moment</source>
        <translation type="vanished">认证中，请稍后</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">密码错误，请重试</translation>
    </message>
    <message>
        <source>ResetPWD?</source>
        <translation type="vanished">ཡང་བསྐྱར་བཀོད་སྒྲིག་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>SetNewUEduPWD</source>
        <translation type="vanished">SetNewUeduPWD</translation>
    </message>
    <message>
        <source>ConfirmNewUEduPWD</source>
        <translation type="vanished">གསར་དུ་གཏན་འཁེལ་བྱས་པའི་ནུའུ་ཝུའུ་ཏུའུ་ཕུའུ་ཝེ་ཏི།</translation>
    </message>
    <message>
        <source>The two password entries are inconsistent, please reset</source>
        <translation type="vanished">གསང་བའི་འཇུག་སྒོ་གཉིས་གཅིག་མཐུན་མིན་པས་བསྐྱར་དུ་བཀོད་སྒྲིག་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Password entered incorrectly, please try again</source>
        <translation type="vanished">གསང་གྲངས་ནང་འཇུག་བྱས་པ་ནོར་འདུག་པས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་གནང་རོགས།</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">གསལ་པོར་བཤད་ན།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA 及 WPA2 企业</translation>
    </message>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">མིང་མ་བཀོད་པའི་ཐོབ་ཐང</translation>
    </message>
    <message>
        <source>Allow automatic PAC pro_visioning</source>
        <translation type="vanished">རང་འགུལ་གྱིས་PACའགོ་pro_visioning་བྱས་ཆོག།</translation>
    </message>
    <message>
        <source>PAC file</source>
        <translation type="vanished">PACཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <source>Anonymous</source>
        <translation type="vanished">མིང་མ་བཀོད་པའི་</translation>
    </message>
    <message>
        <source>Authenticated</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་བྱས།</translation>
    </message>
    <message>
        <source>Both</source>
        <translation type="vanished">དེ་གཉིས་ཀ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">མིང་མ་བཀོད་པའི་ཐོབ་ཐང</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA དཔང་ཡིག</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <source>PEAP version</source>
        <translation type="vanished">PEAPཔར་གཞི།</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ཡིག་ཆའི་ནང་ནས་གདམ་གསེས་</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">རང་འགུལ་གྱིས་རང་</translation>
    </message>
    <message>
        <source>Version 0</source>
        <translation type="vanished">པར་གཞི་0</translation>
    </message>
    <message>
        <source>Version 1</source>
        <translation type="vanished">པར་གཞི་དང་པོ།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="vanished">ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA དཔང་ཡིག</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <source>User certificate password</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་ལག་ཁྱེར་གྱི་གསང་</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་ལྡེ་མིག་གི་གསང་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ཡིག་ཆའི་ནང་ནས་གདམ་གསེས་</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">མིང་མ་བཀོད་པའི་ཐོབ་ཐང</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA དཔང་ཡིག</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ཡིག་ཆའི་ནང་ནས་གདམ་གསེས་</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Key</source>
        <translation type="vanished">ལྡེ་མིག</translation>
    </message>
    <message>
        <source>WEP index</source>
        <translation type="vanished">WEPསྟོན་གྲངས།</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passprase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <source>1(default)</source>
        <translation type="vanished">1(default)</translation>
    </message>
    <message>
        <source>Open System</source>
        <translation type="vanished">སྒོ་འབྱེད་མ་ལག</translation>
    </message>
    <message>
        <source>Shared Key</source>
        <translation type="vanished">མཉམ་སྤྱོད་ཀྱི་ལྡེ་མིག</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད་</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">C_reate…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA 及 WPA2 企业</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Create Hotspot</source>
        <translation type="vanished">ཚ་བ་ཆེ་བའི་ས་ཆ་གསར་སྐྲུན་</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 མི་སྒེར་</translation>
    </message>
</context>
<context>
    <name>EngineDevice</name>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="284"/>
        <source>%1% available, charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="290"/>
        <source>Left %1h %2m (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="295"/>
        <source>%1% available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="301"/>
        <source>Left %1h %2m to full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="303"/>
        <source>charging (%1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="309"/>
        <source>%1 waiting to discharge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="314"/>
        <source>%1 waiting to charge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="334"/>
        <source>AC adapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="338"/>
        <source>Laptop battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="342"/>
        <source>UPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="346"/>
        <source>Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="350"/>
        <source>Mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="354"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="358"/>
        <source>PDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="362"/>
        <source>Cell phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="366"/>
        <source>Media player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="370"/>
        <source>Tablet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="374"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="378"/>
        <source>unrecognised</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../src/widgets/iconedit.cpp" line="92"/>
        <source>OK</source>
        <translation>སྤྲོད་དགོས།</translation>
    </message>
</context>
<context>
    <name>InputInfos</name>
    <message>
        <source>Service exception...</source>
        <translation type="vanished">ཞབས་ཞུའི་དམིགས་བསལ་ ...</translation>
    </message>
    <message>
        <source>Invaild parameters...</source>
        <translation type="vanished">གོ་མི་ཐུབ་པའི་གྲངས་ཀ་ ...</translation>
    </message>
    <message>
        <source>Unknown fault:%1</source>
        <translation type="vanished">ཤེས་མེད་པའི་ནོར་འཛོལ། %1</translation>
    </message>
    <message>
        <source>Recapture(60s)</source>
        <translation type="vanished">ཕྱིར་ལེན་པ།(60s)</translation>
    </message>
    <message>
        <source>Recapture(%1s)</source>
        <translation type="vanished">ཕྱིར་ལེན་པ།(%1s)</translation>
    </message>
    <message>
        <source>Get code</source>
        <translation type="vanished">ཚབ་རྟགས་ཐོབ་པ།</translation>
    </message>
</context>
<context>
    <name>KBTitle</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="46"/>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="83"/>
        <source>Suspended state</source>
        <translation>གཡེང་བའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="57"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="86"/>
        <source>Welt status</source>
        <translation>རྣམ་པ་མཐའ་གཅིག་ཏུ་སྦྱར་བ།</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <source>KeyboardWidget</source>
        <translation type="vanished">མཐེབ་གཞོང་Widget</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">麒麟网络工具信息提示</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <source>kylin-nm</source>
        <translation type="vanished">kylin-nm</translation>
    </message>
    <message>
        <source>LAN</source>
        <translation type="vanished">ལན་ལན།</translation>
    </message>
    <message>
        <source>Enabel LAN List</source>
        <translation type="obsolete">其他有线网络</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation type="obsolete">无线网络</translation>
    </message>
    <message>
        <source>Enabel WiFi List</source>
        <translation type="obsolete">其他无线网络</translation>
    </message>
    <message>
        <source>New WiFi</source>
        <translation type="obsolete">加入其他网络</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">已开启</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已关闭</translation>
    </message>
    <message>
        <source>HotSpot</source>
        <translation type="vanished">ཚ་བ་ཆེ་བ།</translation>
    </message>
    <message>
        <source>FlyMode</source>
        <translation type="vanished">འཕུར་སྐྱོད་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="vanished">显示网络连接界面</translation>
    </message>
    <message>
        <source>Inactivated LAN</source>
        <translation type="vanished">འགུལ་སྐྱོད་མ་བྱས་པའི་LAN</translation>
    </message>
    <message>
        <source>Inactivated WLAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <source>Other WLAN</source>
        <translation type="vanished">WLAN གཞན་དག</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <source>Show KylinNM</source>
        <translation type="vanished">ཅིན་ལིན་ནའེ་མུའུ་ལ་བསྟན་དོན།</translation>
    </message>
    <message>
        <source>No wireless card detected</source>
        <translation type="vanished">སྐུད་མེད་བྱང་བུ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <source>Activated LAN</source>
        <translation type="vanished">འགུལ་སྐྱོད་བྱས་པའི་LAN</translation>
    </message>
    <message>
        <source>Activated WLAN</source>
        <translation type="vanished">འགུལ་སྐྱོད་བྱས་པའི་WLAN</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="vanished">འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <source>No Other Wired Network Scheme</source>
        <translation type="vanished">སྐུད་ཡོད་བརྙན་འཕྲིན་དྲ་བའི་འཆར་གཞི་གཞན་དག་མེད</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>No wifi connected.</source>
        <translation type="obsolete">未连接任何网络</translation>
    </message>
    <message>
        <source>No Other Wireless Network Scheme</source>
        <translation type="vanished">སྐུད་མེད་དྲ་རྒྱའི་འཆར་གཞི་གཞན་དག་མེད།</translation>
    </message>
    <message>
        <source>Wired net is disconnected</source>
        <translation type="vanished">སྐུད་ཡོད་དྲ་རྒྱ་ཆད་པ།</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="obsolete">断开无线网络</translation>
    </message>
    <message>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation type="vanished">ཁྱོད་ཀྱི་Wi-Fiཡི་གསང་གྲངས་སམ་ཡང་ན་སྐུད་མེད་བྱང་བུ་བཀོལ་སྤྱོད་བྱེད་ཆོག་པ་ར་སྤྲོད</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">其他有线网络</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">新建有线网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>No usable network in the list</source>
        <translation type="vanished">མིང་ཐོའི་ནང་དུ་སྤྱོད་གོ་ཆོད་པའི་དྲ་རྒྱ་མེད།</translation>
    </message>
    <message>
        <source>NetOn,</source>
        <translation type="vanished">དྲ་རྒྱ་དང་།</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">其他无线网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">打开无线网开关前保持有线网开关打开</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">请先插入无线网卡</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">正在断开异常连接的网络</translation>
    </message>
    <message>
        <source>update Wi-Fi list now, click again</source>
        <translation type="vanished">正在更新 Wi-Fi列表 请再次点击</translation>
    </message>
    <message>
        <source>update Wi-Fi list now</source>
        <translation type="vanished">正在更新 Wi-Fi列表</translation>
    </message>
    <message>
        <source>Conn Ethernet Success</source>
        <translation type="vanished">Conn Ethernetལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Conn Ethernet Fail</source>
        <translation type="vanished">ཁུང་ནེ་ཨེ་ཐེ་ནའེ་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Conn Wifi Success</source>
        <translation type="vanished">ཁུང་ནེ་ཝེ་ཧྥེ་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="172"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="186"/>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightDMHelper</name>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="121"/>
        <source>failed to start session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="215"/>
        <source>Login</source>
        <translation type="unfinished">ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="224"/>
        <source>Guest</source>
        <translation type="unfinished">游客</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <source>Date</source>
        <translation type="vanished">དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="514"/>
        <source>SwitchUser</source>
        <translation>སྤྱོད་མཁན་བརྗེ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="581"/>
        <source>Power</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="568"/>
        <source>VirtualKeyboard</source>
        <translation>རྟོག་བཟོའི་མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">སྤྱོད་མཁན་མང་པོ་ཞིག་དུས་གཅིག་ཏུ་ཐོ་འགོད་བྱས་པ་རེད། ཁྱོད་ཀྱིས་མ་ལག་འདི་བསྐྱར་དུ་སྒྲིག་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>LAN</source>
        <translation type="vanished">སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="483"/>
        <source>SwitchSession</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="501"/>
        <source>Power Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="544"/>
        <source>Network</source>
        <translation type="unfinished">网络</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="625"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1727"/>
        <source>system-monitor</source>
        <translation>མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1253"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1259"/>
        <source>%1 may cause users who have logged in to this computer to lose content that has not yet been stored,To still perform please click &quot;Confirm&quot;.</source>
        <translation>%1རྩིས་འཁོར་དེ་ཉིད་ཐོ་འགོད་བྱས་ཟིན་པའི་སྤྱད་མཁན་གྱིས་ད་དུང་གསོག་ཉར་བྱས་མེད་པའི་ནང་དོན་བསླངས་ནས་མུ་མཐུད་དུ་ལག་བསྟར་བྱེད་དགོས་པ་དང་། &quot;དངོས་སུ་ཁས་ལེན་&quot;བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1255"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1290"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1289"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1294"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to %1 this system?</source>
        <translation>སྤྱོད་མཁན་མང་པོ་ཞིག་དུས་གཅིག་ཏུ་ཐོ་འགོད་བྱས། ཁྱེད་རང་གིས་%1མ་ལག་འདི་གཏན་འཁེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">བསྐྱར་དུ་སྒོ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <source>PowerOff</source>
        <translation type="vanished">གནམ་གྲུའི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1261"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1295"/>
        <source>Shut Down</source>
        <translation>ལས་མཚམས་འཇོག</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="82"/>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="105"/>
        <source>Login Options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="289"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="315"/>
        <source>Other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1034"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1036"/>
        <source>FingerVein</source>
        <translation>སྡོད་རྩ་ཞེས་པ་ནི་སྡོད་རྩར</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1038"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1040"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1042"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1044"/>
        <source>Ukey</source>
        <translation>བདེ་འཇགས་གསང་ལྡེ་</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1046"/>
        <source>QRCode</source>
        <translation>དོན་ཚན་གཉིས་པ།</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="703"/>
        <source>Identify device removed!</source>
        <translation>དབྱེ་འབྱེད་སྒྲིག་ཆས་མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>MPRISWidget</name>
    <message>
        <location filename="../src/widgets/mpriswidget.cpp" line="214"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="215"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="228"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="237"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <source>Verification code</source>
        <translation type="vanished">ཞིབ་བཤེར་གྱི་ཚབ་རྟགས།</translation>
    </message>
</context>
<context>
    <name>MyNetworkWidget</name>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="52"/>
        <source>LAN</source>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="54"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="161"/>
        <source>&amp;&amp;?!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="175"/>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="310"/>
        <source>Return</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <source>Form</source>
        <translation type="vanished">རྣམ་པ།</translation>
    </message>
    <message>
        <source>Automatically join the network</source>
        <translation type="vanished">རང་འགུལ་གྱིས་དྲ་རྒྱའི་ནང་དུ་ཞུགས་པ།</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <source>Input Password...</source>
        <translation type="vanished">ནང་འཇུག་གི་གསང་གྲངས་</translation>
    </message>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Signal：</source>
        <translation type="vanished">བརྡ་རྟགས་ནི།</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">开放</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">安全</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">速率</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <source>WiFi Security：</source>
        <translation type="vanished">WiFiབཀོལ་སྤྱོད་བདེ་འཇགས་ནི།</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="vanished">MAC:</translation>
    </message>
    <message>
        <source>Conn Wifi Failed</source>
        <translation type="vanished">ཁུང་ནེ་ཝེ་ཧྥེ་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <source>Form</source>
        <translation type="vanished">རྣམ་པ།</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <source>No Configuration</source>
        <translation type="vanished">བཀོད་སྒྲིག་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>IPv4：</source>
        <translation type="vanished">IPv4:</translation>
    </message>
    <message>
        <source>IPv6：</source>
        <translation type="vanished">IPv6:</translation>
    </message>
    <message>
        <source>BandWidth：</source>
        <translation type="vanished">带宽:</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="vanished">MAC:</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">自动</translation>
    </message>
</context>
<context>
    <name>PhoneAuthWidget</name>
    <message>
        <source>Verification by phoneNum</source>
        <translation type="vanished">ཁ་པར་གྱི་ནུའུ་མུའུ་ཡིས་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <source>「 Use bound Phone number to verification 」</source>
        <translation type="vanished">「ས་མཚམས་ཀྱི་ཁ་པར་ཨང་གྲངས་ལ་བརྟེན་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <source>「 Use SMS to verification 」</source>
        <translation type="vanished">「SMSབཀོལ་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <source>commit</source>
        <translation type="vanished">བསྒྲུབ་རྒྱུ་ཁས་ལེན་</translation>
    </message>
    <message>
        <source>Network not connected~</source>
        <translation type="vanished">དྲ་རྒྱ་དང་འབྲེལ་མཐུད་མི་བྱེད་པ</translation>
    </message>
    <message>
        <source>Network unavailable~</source>
        <translation type="vanished">དྲ་རྒྱ་སྤྱོད་མི་ཐུབ་པ~</translation>
    </message>
    <message>
        <source>Verification Code invalid!</source>
        <translation type="vanished">ཞིབ་བཤེར་ཚད་གཞི་གོ་མི་ཆོད་པ་རེད།</translation>
    </message>
    <message>
        <source>Verification Code incorrect.Please retry!</source>
        <translation type="vanished">ཞིབ་བཤེར་ཚད་གཞི་ཡང་དག་མིན་པ། ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་ཞིབ</translation>
    </message>
    <message>
        <source>Failed time over limit!Retry after 1 hour!</source>
        <translation type="vanished">ཚད་ལས་བརྒལ་ནས་ཕམ་ཁ་བྱུང་བའི་དུས་ཚོད་ ཆུ་ཚོད་1འགོར་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>verifaction failed!</source>
        <translation type="vanished">ངོ་རྒོལ་བྱེད་པའི་བྱ་སྤྱོད་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>PowerListWidget</name>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="299"/>
        <location filename="../src/widgets/powerlistwidget.h" line="109"/>
        <source>Hibernate</source>
        <translation>ཧིན་རྡུ་ཉི་ཞི་ཡ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="300"/>
        <location filename="../src/widgets/powerlistwidget.h" line="110"/>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation>གློག་ཀླད་ཀྱི་སྒོ་བརྒྱབ་ནས་བཀོལ་སྤྱོད་བྱེད་སྐབས་ཐོག་མཐའ་བར་གསུམ་དུ་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ། གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་དུས་ཁྱོད་དང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="303"/>
        <location filename="../src/widgets/powerlistwidget.h" line="122"/>
        <source>Suspend</source>
        <translation>གནས་སྐབས་མཚམས་འཇོག་</translation>
    </message>
    <message>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="vanished">གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་བའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱས་མོད། འོན་ཀྱང་གློག་ཟད་ཚད་ཅུང་ཉུང་། ཉེར་སྤྱོད་ཚོགས་འདུའི་ཐོག་ཁ་ཕྱེ་ནས་མགྱོགས་མྱུར་ངང་གློག་ཀླད་དཀྲོགས་ཏེ་ཁྱོད་དང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="307"/>
        <location filename="../src/widgets/powerlistwidget.h" line="172"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="308"/>
        <source>Close all apps, and then restart your computer</source>
        <translation>བཀོལ་སྤྱོད་ཡོད་ཚད་སྒོ་བརྒྱབ་རྗེས་གློག་ཀླད་བསྐྱར་དུ་སྒོ་ཕྱེ་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="310"/>
        <location filename="../src/widgets/powerlistwidget.h" line="196"/>
        <source>Shut Down</source>
        <translation>ལས་མཚམས་འཇོག</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="315"/>
        <location filename="../src/widgets/powerlistwidget.h" line="149"/>
        <source>The current user logs out of the system, terminates the session, and returns to the login page</source>
        <translation>མིག་སྔར་སྤྱོད་མཁན་གྱི་ཐོ་ཁོངས་ནས་སུབ་པ་དང་། ཚོགས་འདུ་གྲོལ་མཚམས་བཞག་ནས་ཤོག་ངོས་སུ་འགོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="325"/>
        <source>Logout</source>
        <translation>ཐོ་ཁོངས་ནས་སུབ་དགོས།</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="vanished">གློག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="311"/>
        <location filename="../src/widgets/powerlistwidget.h" line="197"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>བཀོལ་སྤྱོད་ཡོད་ཚད་སྒོ་བརྒྱབ་ནས་གློག་ཀླད་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="317"/>
        <location filename="../src/widgets/powerlistwidget.h" line="97"/>
        <source>SwitchUser</source>
        <translation>སྤྱོད་མཁན་བརྗེ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="319"/>
        <location filename="../src/widgets/powerlistwidget.h" line="135"/>
        <source>LockScreen</source>
        <translation>ཟྭ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="313"/>
        <location filename="../src/widgets/powerlistwidget.h" line="147"/>
        <source>Log Out</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="304"/>
        <location filename="../src/widgets/powerlistwidget.h" line="123"/>
        <source>The computer stays on, but consumes less power, The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་བའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱས་མོད། འོན་ཀྱང་གློག་ཟད་ཚད་ཅུང་ཉུང་། ཉེར་སྤྱོད་ཚོགས་འདུའི་ཐོག་ཁ་ཕྱེ་ནས་མགྱོགས་མྱུར་ངང་གློག་ཀླད་དཀྲོགས་ཏེ་ཁྱོད་དང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="321"/>
        <location filename="../src/widgets/powerlistwidget.h" line="160"/>
        <source>UpgradeThenRestart</source>
        <translation>གསར་བཅོས་བྱས་རྗེས་བསྐྱར་དུ་སྒོ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.h" line="173"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>སྤྱོད་སྒོ་ཡོད་ཚད་སྒོ་བརྒྱབ་ནས་གློག་ཀླད་སྒོ་རྒྱག་པ་དང་། དེ་ནས་ཡང་བསྐྱར་གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="323"/>
        <location filename="../src/widgets/powerlistwidget.h" line="184"/>
        <source>UpgradeThenShutdown</source>
        <translation>རྒྱབ་སྒོ་གསར་སྒྱུར་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <source>lock</source>
        <translation type="vanished">ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation type="vanished">ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="vanished">གློག་ཆད་པ།</translation>
    </message>
    <message>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation type="vanished">སྤྱོད་སྒོ་ཡོད་ཚད་སྒོ་བརྒྱབ་ནས་གློག་ཀླད་སྒོ་རྒྱག་པ་དང་། དེ་ནས་ཡང་བསྐྱར་གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་བ་རེད།</translation>
    </message>
    <message>
        <source>Close all apps, and then shut down your computer</source>
        <translation type="vanished">བཀོལ་སྤྱོད་ཡོད་ཚད་སྒོ་བརྒྱབ་ནས་གློག་ཀླད་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="vanished">ལས་མཚམས་འཇོག</translation>
    </message>
    <message>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation type="vanished">གློག་ཀླད་ཀྱི་སྒོ་བརྒྱབ་ནས་བཀོལ་སྤྱོད་བྱེད་སྐབས་ཐོག་མཐའ་བར་གསུམ་དུ་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ། གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་དུས་ཁྱོད་དང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">ཧིན་རྡུ་ཉི་ཞི་ཡ།</translation>
    </message>
    <message>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="vanished">གློག་ཀླད་ཀྱི་ཁ་ཕྱེ་བའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱས་མོད། འོན་ཀྱང་གློག་ཟད་ཚད་ཅུང་ཉུང་། ཉེར་སྤྱོད་ཚོགས་འདུའི་ཐོག་ཁ་ཕྱེ་ནས་མགྱོགས་མྱུར་ངང་གློག་ཀླད་དཀྲོགས་ཏེ་ཁྱོད་དང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">གནས་སྐབས་མཚམས་འཇོག་</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="108"/>
        <source>The screensaver is active.</source>
        <translation>བརྙན་ཤེལ་འདི་འགུལ་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="110"/>
        <source>The screensaver is inactive.</source>
        <translation>བརྙན་ཤེལ་འདི་ལ་ནུས་པ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="28"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="30"/>
        <source>FingerVein</source>
        <translation>སྡོད་རྩ་ཞེས་པ་ནི་སྡོད་རྩར</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="32"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="34"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="36"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="38"/>
        <source>Ukey</source>
        <translation type="unfinished">བདེ་འཇགས་གསང་ལྡེ་</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="40"/>
        <source>QRCode</source>
        <translation>དོན་ཚན་གཉིས་པ།</translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="308"/>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">退出(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="136"/>
        <source>Picture does not exist</source>
        <translation>རི་མོ་མི་གནས་པ།</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="vanished">设置为桌面壁纸</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="vanished">自动切换</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="obsolete">您有%1条未读消息</translation>
    </message>
    <message>
        <source>You have new notification</source>
        <translation type="vanished">ཁྱེད་ཚོར་བརྡ་ཐོ་གསར་པ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="139"/>
        <source>View</source>
        <translation>ལྟ་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <location filename="../src/screensaver/sleeptime.cpp" line="75"/>
        <source>You have rested</source>
        <translation>ཁྱེད་ཚོས་ངལ་གསོ་བྱས་ཟིན་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <source>You have rested:</source>
        <translation type="vanished">ཁྱེད་ཚོས་ངལ་གསོ་བྱས་ཟིན་པ་གཤམ་གསལ།</translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <source>Form</source>
        <translation type="vanished">རྣམ་པ།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation type="vanished">གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གནས་སྐབས་མཚམས་འཇོག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation type="vanished">གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་ལ་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation type="vanished">གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གི་སྒོ་རྒྱག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation type="vanished">གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་བསྐྱར་དུ་འབྱུང་བར་སྔོན་འགོག་བྱེད་ཆེད་ཡིན།</translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>login by password</source>
        <translation type="vanished">密码登录</translation>
    </message>
    <message>
        <source>login by qr code</source>
        <translation type="vanished">微信登录</translation>
    </message>
</context>
<context>
    <name>SwitchButtonGroup</name>
    <message>
        <source>uEduPWD</source>
        <translation type="vanished">uEduPWD</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">འཕྲིན་ཕྲན།</translation>
    </message>
</context>
<context>
    <name>TabletLockWidget</name>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>New password is the same as old</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་དང་རྙིང་པ་གཅིག་མཚུངས་ཡིན།</translation>
    </message>
    <message>
        <source>Reset password error:%1</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་ནོར་འཁྲུལ་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།%1</translation>
    </message>
    <message>
        <source>Please scan by correct WeChat</source>
        <translation type="vanished">ཡང་དག་པའི་འཕྲིན་ཕྲན་ཐོག་ནས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation type="vanished">བྲོས་བྱོལ་དུ་སོང་བ།</translation>
    </message>
</context>
<context>
    <name>UserListWidget</name>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="67"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="121"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="69"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="123"/>
        <source>Guest</source>
        <translation type="unfinished">游客</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="vanished">kylinདྲ་རྒྱའི་ཀུ་ཤུའི་ཅོག་ཙེའི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>VerificationWidget</name>
    <message>
        <source>Please scan by bound WeChat</source>
        <translation type="vanished">འཕྲིན་ཕྲན་གྱིས་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>VerticalVerificationWidget</name>
    <message>
        <source>Please scan by bound WeChat</source>
        <translation type="vanished">འཕྲིན་ཕྲན་གྱིས་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>WeChatAuthDialog</name>
    <message>
        <source>Login by wechat</source>
        <translation type="vanished">ཝེ་ཆི་ཐེས་ཐོ་འགོད་བྱས་པ།</translation>
    </message>
    <message>
        <source>Verification by wechat</source>
        <translation type="vanished">ཝེ་ཆི་ཐེས་ཞིབ་བཤེར་བྱས་པ།</translation>
    </message>
    <message>
        <source>「 Use registered WeChat account to login 」</source>
        <translation type="vanished">「ཐོ་འགོད་བྱས་ཟིན་པའི་སྐད་འཕྲིན་གྱི་ཐོ་ཁོངས་ལ་བརྟེན་ནས་ཐོ་འགོད་བྱེད་པ།」</translation>
    </message>
    <message>
        <source>「 Use bound WeChat account to verification 」</source>
        <translation type="vanished">「ཚོད་འཛིན་རང་བཞིན་གྱི་སྐད་འཕྲིན་ཨང་གྲངས་ལ་བརྟེན་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <source>Network not connected~</source>
        <translation type="vanished">དྲ་རྒྱ་དང་འབྲེལ་མཐུད་མི་བྱེད་པ</translation>
    </message>
    <message>
        <source>Scan code successfully</source>
        <translation type="vanished">ཞིབ་བཤེར་ཨང་གྲངས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Timeout!Try again!</source>
        <translation type="vanished">དུས་ཚོད་ཕྱིར་འགོར་སོང་། ཡང་བསྐྱར་ཚོད་ལྟ།!</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation type="vanished">登录失败</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="14"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="26"/>
        <source>LoadPlugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>action</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="76"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="81"/>
        <source>which block type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="69"/>
        <source>which block type,param:Suspend/Hibernate/Restart/Shutdown/Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="74"/>
        <source>which block type,param:Shutdown/Restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="62"/>
        <source>how long to show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="67"/>
        <source>if show lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="44"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>ukui ScreenSaver ལ་བཀོད་འདོམས་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="49"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="46"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="48"/>
        <source>lock the screen immediately</source>
        <translation>འཕྲལ་དུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="52"/>
        <source>query the status of the screen saver</source>
        <translation>བརྙན་ཤེལ་གསོག་འཇོག་བྱེད་མཁན་གྱི་གནས་ཚུལ་ལ་འདྲི་རྩད་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="55"/>
        <source>unlock the screen saver</source>
        <translation>བརྙན་ཤེལ་གསོག་འཇོག་བྱེད་མཁན་ལ་སྒོ་འབྱེད་</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="57"/>
        <source>show the screensaver</source>
        <translation>བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་བལྟ་རུ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="60"/>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="63"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="70"/>
        <source>show the session tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="65"/>
        <source>show the switchuser window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="68"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="75"/>
        <source>show the app block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="73"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="80"/>
        <source>show the multiUsers block window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="40"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>དབྱིན་ཇིའི་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་ཁ་པར་རྒྱག་པའི་གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="50"/>
        <source>activated by session idle signal</source>
        <translation>སྐབས་འདིའི་ཁོམ་པའི་བརྡ་རྟགས་ཀྱིས་སྐུལ་སློང་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="53"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="58"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation>བརྙན་ཤེལ་ལ་ཟྭ་བརྒྱབ་ནས་འཕྲལ་མར་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་སྟོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="55"/>
        <source>show screensaver immediately</source>
        <translation>འཕྲལ་མར་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་སྟོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="61"/>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="66"/>
        <source>show blank screensaver immediately and if lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="72"/>
        <source>show switch user window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="63"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation>དབྱིན་ཇིའི་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་ལྟ་ཞིབ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="67"/>
        <source>show on root window</source>
        <translation>རྩ་བའི་སྒེའུ་ཁུང་ནས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="69"/>
        <source>show on window.</source>
        <translation>སྒེའུ་ཁུང་ནས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="70"/>
        <source>window id</source>
        <translation>སྒེའུ་ཁུང་གི་ཐོབ་ཐང་</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="56"/>
        <source>Backend for the ukui ScreenSaver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="62"/>
        <source>lock the screen by startup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
