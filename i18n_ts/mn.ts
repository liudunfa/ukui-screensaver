<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../src/widgets/agreementwindow.cpp" line="47"/>
        <source>I know</source>
        <translation>ᠪᠢ ᠮᠡᠳᠡᠵᠡᠢ</translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">选择其他设备</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">使用生物识别认证</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">使用密码认证</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1173"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">解锁</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">已登录</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1244"/>
        <source>Please try again in %1 minutes.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1251"/>
        <source>Please try again in %1 seconds.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1258"/>
        <source>Account locked permanently.</source>
        <translation>ᠳᠠᠩᠰᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠨᠢᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="652"/>
        <location filename="../src/widgets/authdialog.cpp" line="680"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>ᠨᠢᠭᠤᠷ ᠱᠢᠷᠪᠢᠵᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="458"/>
        <source>Guest</source>
        <translation>ᠵᠤᠷᠴᠢᠭᠴᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="655"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ ᠳᠠᠷᠤᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="658"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>ᠳᠠᠭᠤ᠎ᠪᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="661"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="664"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ᠎ᠶᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="934"/>
        <source>Password:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="937"/>
        <source>Input Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="942"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1012"/>
        <source>User name input error!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠪᠤᠷᠤᠭᠤ ᠤᠷᠤᠭᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1091"/>
        <source>login</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1477"/>
        <location filename="../src/widgets/authdialog.cpp" line="1627"/>
        <location filename="../src/widgets/authdialog.cpp" line="1771"/>
        <location filename="../src/widgets/authdialog.cpp" line="1952"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1480"/>
        <location filename="../src/widgets/authdialog.cpp" line="1630"/>
        <location filename="../src/widgets/authdialog.cpp" line="1776"/>
        <location filename="../src/widgets/authdialog.cpp" line="1779"/>
        <location filename="../src/widgets/authdialog.cpp" line="1955"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠶᠢ/᠎ᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1808"/>
        <source>Abnormal network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1816"/>
        <source>Face recognition waiting time out, please click refresh or enter the password to unlock.</source>
        <translation>ᠬᠦᠮᠦᠨ ᠤ ᠨᠢᠭᠤᠷ ᠢ ᠢᠯᠭᠠᠬᠤ ᠦᠶᠡᠰ᠂ ᠰᠢᠨᠡᠳᠭᠡᠬᠦ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠤᠨ ᠣᠨᠢᠰᠤ ᠶᠢ ᠲᠠᠷᠤᠭᠠᠷᠠᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2079"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠣᠷᠣᠮ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2081"/>
        <source>FingerVein</source>
        <translation>ᠨᠠᠮ ᠵᠢᠮ ᠰᠤᠳᠠᠯ ᠢ ᠵᠢᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2083"/>
        <source>Iris</source>
        <translation>ᠬᠠᠯᠢᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2085"/>
        <source>Face</source>
        <translation>ᠴᠢᠷᠠᠢ᠎ᠪᠠᠷ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2087"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠣᠷᠣᠮ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2089"/>
        <location filename="../src/widgets/authdialog.cpp" line="2111"/>
        <source>Ukey</source>
        <translation>ᠠᠶᠤᠯᠭᠦᠢ ᠨᠢᠭᠤᠴᠠ ᠲᠦᠯᠬᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2091"/>
        <location filename="../src/widgets/authdialog.cpp" line="2113"/>
        <source>QRCode</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2101"/>
        <source>fingerprint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠣᠷᠣᠮ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2103"/>
        <source>fingervein</source>
        <translation>ᠨᠠᠮ ᠵᠢᠮ ᠰᠤᠳᠠᠯ ᠢ ᠵᠢᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2105"/>
        <source>iris</source>
        <translation>ᠬᠠᠯᠢᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2107"/>
        <source>face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="2109"/>
        <source>voiceprint</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠣᠷᠣᠮ</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="791"/>
        <source>Password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1793"/>
        <location filename="../src/widgets/authdialog.cpp" line="1797"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>%1᠎ᠶᠢᠨ/᠎ᠦᠨ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠ ᠪᠠᠰᠠ%2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ ᠳᠤᠷᠱᠢᠬᠤ ᠵᠠᠪᠱᠢᠶᠠᠨ ᠲᠠᠢ</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">密码错误，请重试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩%1次尝试机会</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="1015"/>
        <location filename="../src/widgets/authdialog.cpp" line="1019"/>
        <source>Authentication failure, Please try again</source>
        <translation>ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="667"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋ ᠱᠢᠷᠪᠢᠬᠦ᠌ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Enter the ukey password</source>
        <translation type="vanished">ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Insert the ukey into the USB port</source>
        <translation type="vanished">ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠨᠢᠭᠤᠴᠠ ᠶᠢ USB ᠦᠵᠦᠭᠦᠷ ᠲᠦ ᠬᠠᠳᠬᠤᠵᠤ ᠣᠷᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Password </source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ </translation>
    </message>
    <message>
        <location filename="../src/widgets/authdialog.cpp" line="456"/>
        <location filename="../src/widgets/authdialog.cpp" line="1165"/>
        <source>Login</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>BatteryWidget</name>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="112"/>
        <location filename="../src/widgets/batterywidget.cpp" line="142"/>
        <source>Charging...</source>
        <translation>ᠶᠠᠭ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠵᠦ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="114"/>
        <location filename="../src/widgets/batterywidget.cpp" line="144"/>
        <source>fully charged</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠳᠦᠭᠦᠷᠡᠩ ᠪᠣᠯᠵᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="118"/>
        <location filename="../src/widgets/batterywidget.cpp" line="148"/>
        <source>PowerMode</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/widgets/batterywidget.cpp" line="121"/>
        <location filename="../src/widgets/batterywidget.cpp" line="151"/>
        <source>BatteryMode</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠢ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">请选择其他生物识别设备</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">设备名称：</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <source>Current device: </source>
        <translation type="vanished">ᠤᠳᠤᠬᠠᠨ᠎ᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ </translation>
    </message>
    <message>
        <source>Identify failed, Please retry.</source>
        <translation type="vanished">ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <source>Please select the biometric device</source>
        <translation type="vanished">ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ᠎ᠤᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Device type:</source>
        <translation type="vanished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠳᠦᠷᠰᠦ ᠄</translation>
    </message>
    <message>
        <source>Device name:</source>
        <translation type="vanished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢᠨ ᠱᠢᠨᠵᠢ ᠳᠡᠮᠳᠡᠭ ᠄</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>BlockWidget</name>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="60"/>
        <location filename="../src/widgets/blockwidget.cpp" line="140"/>
        <location filename="../src/widgets/blockwidget.cpp" line="239"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="64"/>
        <location filename="../src/widgets/blockwidget.cpp" line="141"/>
        <location filename="../src/widgets/blockwidget.cpp" line="240"/>
        <source>Confrim</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="147"/>
        <location filename="../src/widgets/blockwidget.cpp" line="166"/>
        <source>If you do not perform any operation, the system will automatically %1 after %2 seconds.</source>
        <translation>ᠬᠡᠷᠪᠡ ᠲᠠ ᠶᠠᠮᠠᠷ ᠴᠤ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠦᠭᠡᠢ ᠪᠣᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ %2 ᠰᠧᠺᠦᠢᠨᠳ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠦᠢᠲ᠋ᠣᠴᠢᠯᠠᠨ %1 ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="181"/>
        <source>The following programs prevent restarting, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠳᠤ ᠲᠠ 《 ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ 》 ᠶᠢ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠳᠡᠭᠡᠷ ᠫᠷᠦᠭ᠍ᠷᠠᠮ ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="185"/>
        <source>The following programs prevent the shutdown, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠨᠢ ᠬᠠᠭᠠᠯᠭ᠎ᠠ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠳᠤ ᠲᠠ 《 ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ 》 ᠶᠢ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠳᠡᠭᠡᠷ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="190"/>
        <source>The following programs prevent suspend, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤᠨ ᠨᠣᠶᠢᠷ ᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠳᠤ ᠲᠠ 《 ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ 》 ᠶᠢ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠳᠡᠭᠡᠷ ᠫᠷᠦᠭ᠍ᠷᠠᠮ ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="193"/>
        <source>The following programs prevent hibernation, you can click &quot;Cancel&quot; and then close these programs.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢᠶᠠᠷ ᠤᠨᠲᠠᠬᠤ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠳᠤ ᠲᠠ 《 ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ 》 ᠶᠢ ᠳᠠᠷᠤᠭᠠᠳ ᠡᠳᠡᠭᠡᠷ ᠫᠷᠦᠭ᠍ᠷᠠᠮ ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="197"/>
        <source>The following programs prevent you from logging out, you can click &quot;Cancel&quot; and then close them.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠨᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠳᠤ ᠲᠠ 《 ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ 》 ᠶᠢ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠳᠡᠭᠡᠷ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="247"/>
        <source>shut down</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/blockwidget.cpp" line="250"/>
        <source>restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/charsmorewidget.cpp" line="183"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="115"/>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="273"/>
        <source>More</source>
        <translation>ᠨᠡᠩ ᠠᠷᠪᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="129"/>
        <source>ABC</source>
        <translation>ABC</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/charswidget.cpp" line="142"/>
        <source>123</source>
        <translation>123</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <source>edit network</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>LAN name: </source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄ </translation>
    </message>
    <message>
        <source>Method: </source>
        <translation type="vanished">ᠠᠷᠭ᠎ᠠ ᠄ </translation>
    </message>
    <message>
        <source>Address: </source>
        <translation type="vanished">ᠬᠠᠶᠢᠭ ᠄ </translation>
    </message>
    <message>
        <source>Netmask: </source>
        <translation type="vanished">ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠠᠯᠳᠠᠯᠠᠯ ᠄ </translation>
    </message>
    <message>
        <source>Gateway: </source>
        <translation type="vanished">ᠠᠶᠠᠳᠠᠯ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠪᠤᠭᠤᠮᠳᠠ ᠄ </translation>
    </message>
    <message>
        <source>DNS 1: </source>
        <translation type="vanished">DNS᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ </translation>
    </message>
    <message>
        <source>DNS 2: </source>
        <translation type="vanished">ᠪᠡᠯᠡᠳᠭᠡᠯ ᠰᠤᠩᠭᠤᠭᠤᠯᠢDNS ᠄ </translation>
    </message>
    <message>
        <source>Edit Conn</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <source>Auto(DHCP)</source>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠤ᠋(DHCP)</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">ᠭᠠᠷ᠎ᠢᠶᠠᠷ ᠬᠦᠳᠡᠯᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">Ok</translation>
    </message>
    <message>
        <source>Can not create new wired network for without wired card</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠳᠤᠳᠠᠭᠤ ᠂ ᠱᠢᠨ᠎ᠡ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠭᠤᠯᠬᠤ᠎ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>New network already created</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>New network settings already finished</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠢᠷᠢᠯᠠᠯ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Edit Network</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>Add Wired Network</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">已创建新的有线网络</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">新的设置已经生效</translation>
    </message>
    <message>
        <source>New settings already effective</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>There is a same named LAN exsits.</source>
        <translation type="obsolete">已有同名连接存在</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">ᠴᠢᠷᠠᠢ᠎ᠪᠠᠷ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <source>ukey</source>
        <translation type="vanished">ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ ᠬᠥᠯᠬᠢᠳᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="vanished">ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
</context>
<context>
    <name>DigitalAuthDialog</name>
    <message>
        <source>LoginByUEdu</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>now is authing, wait a moment</source>
        <translation type="vanished">认证中，请稍后</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">密码错误，请重试</translation>
    </message>
    <message>
        <source>ResetPWD?</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠪᠠᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <source>SetNewUEduPWD</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>ConfirmNewUEduPWD</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>The two password entries are inconsistent, please reset</source>
        <translation type="vanished">ᠬᠤᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ᠎ᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Password entered incorrectly, please try again</source>
        <translation type="vanished">ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">ᠬᠤᠭᠤᠰᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ ᠄</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠬᠦᠮᠦᠨ</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA 及 WPA2 企业</translation>
    </message>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭWi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭWi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠤᠨ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯWEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ᠌ ᠄</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <source>Allow automatic PAC pro_visioning</source>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠤ᠋PACᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>PAC file</source>
        <translation type="vanished">PAC ᠹᠠᠢᠯ ᠄</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ （ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ᠎ᠪᠠᠷ ᠳᠠᠪᠱᠢᠬᠤ ᠳᠦᠷᠢᠮ ᠡᠰᠡᠬᠦᠯ᠎ᠡASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠤᠨ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯWEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮTLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠠᠭᠳᠠᠬᠤEAP (PEAP)</translation>
    </message>
    <message>
        <source>Anonymous</source>
        <translation type="vanished">ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Authenticated</source>
        <translation type="vanished">ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Both</source>
        <translation type="vanished">ᠬᠤᠶᠠᠭᠤᠯᠠ᠎ᠶᠢ ᠬᠤᠤᠰᠯᠠᠭᠤᠯᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠠᠭᠳᠠᠬᠤEAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ᠨᠸᠲ᠎ᠦᠨ ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CAᠦᠨᠡᠮᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA ᠦᠨᠡᠮᠯᠡᠯ᠎ᠦᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA ᠦᠨᠡᠮᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>PEAP version</source>
        <translation type="vanished">PEAP ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠤ᠋</translation>
    </message>
    <message>
        <source>Version 0</source>
        <translation type="vanished">ᠬᠡᠪᠯᠡᠯ0</translation>
    </message>
    <message>
        <source>Version 1</source>
        <translation type="vanished">ᠬᠡᠪᠯᠡᠯ1</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="vanished">ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ ᠄</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ᠣᠷᠣᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <source>User certificate password</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠄</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ᠎ᠦᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">ᠣᠷᠣᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Key</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <source>WEP index</source>
        <translation type="vanished">WEPᠡᠷᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>1(default)</source>
        <translation type="vanished">1( ᠠᠶᠠᠳᠠᠯ)</translation>
    </message>
    <message>
        <source>Open System</source>
        <translation type="vanished">ᠨᠡᠬᠡᠬᠡᠯᠳᠡᠳᠡᠢ ᠱᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <source>Shared Key</source>
        <translation type="vanished">ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ᠌ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA 及 WPA2 企业</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Create Hotspot</source>
        <translation type="vanished">ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠬᠠᠯᠠᠮᠱᠢᠯ ᠴᠡᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">Ok</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>EngineDevice</name>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="101"/>
        <source>no</source>
        <translation>ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ（ᠳᠡᠢᠮᠤ）</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="120"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ（ ᠡᠰᠡᠬᠦ᠌᠂ ᠦᠬᠡᠢ）</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="284"/>
        <source>%1% available, charged</source>
        <translation>1% ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠪᠥᠭᠡᠳ ᠰᠦᠢᠳᠬᠡᠯ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="290"/>
        <source>Left %1h %2m (%3%)</source>
        <translation>ᠵᠡᠭᠦᠨ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ 1h%2m (3%)</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="295"/>
        <source>%1% available</source>
        <translation>1% ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="301"/>
        <source>Left %1h %2m to full</source>
        <translation>ᠵᠡᠭᠦᠨ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ 1h% 2m ᠬᠦᠷᠲᠡᠯ᠎ᠡ ᠳᠦᠭᠦᠷᠴᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="303"/>
        <source>charging (%1%)</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠬᠦ (1%)</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="309"/>
        <source>%1 waiting to discharge (%2%)</source>
        <translation>1 ᠡᠮᠨᠡᠯᠭᠡ ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠶᠢ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ (2%)</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="314"/>
        <source>%1 waiting to charge (%2%)</source>
        <translation>1 ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠬᠦ ᠶᠢ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ (2%)</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="334"/>
        <source>AC adapter</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠢ ᠰᠣᠯᠢᠯᠴᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="338"/>
        <source>Laptop battery</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠳᠡᠪᠲᠡᠷ ᠦᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠦᠨ ᠳ᠋ᠢᠶᠠᠨ ᠢ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="342"/>
        <source>UPS</source>
        <translation>UPS ᠶᠢᠨ ᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="346"/>
        <source>Monitor</source>
        <translation>ᠳᠤᠳᠤᠳᠭᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="350"/>
        <source>Mouse</source>
        <translation>ᠮᠠᠦ᠋ᠰ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="354"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶ᠋ᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="358"/>
        <source>PDA</source>
        <translation>ᠳᠥᠭᠥᠮ ᠮᠠᠶᠢᠭ ᠤᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠠᠪᠴᠤ ᠶᠠᠪᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="362"/>
        <source>Cell phone</source>
        <translation>ᠭᠠᠷ ᠤᠲᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="366"/>
        <source>Media player</source>
        <translation>ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤᠨ ᠨᠡᠪᠲᠡᠷᠡᠭᠦᠯᠭᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="370"/>
        <source>Tablet</source>
        <translation>ᠺᠢᠨᠣ᠋</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="374"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="378"/>
        <source>unrecognised</source>
        <translation>ᠢᠯᠭᠠᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../src/widgets/iconedit.cpp" line="92"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>InputInfos</name>
    <message>
        <source>Service exception...</source>
        <translation type="vanished">ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Invaild parameters...</source>
        <translation type="vanished">ᠫᠠᠷᠠᠮᠸᠲᠷ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Unknown fault:%1</source>
        <translation type="vanished">ᠦᠯᠦ᠍ ᠮᠡᠳᠡᠬᠦ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠄%1</translation>
    </message>
    <message>
        <source>Recapture(60s)</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠤᠯᠵᠠᠯᠠᠬᠤ(60s)</translation>
    </message>
    <message>
        <source>Recapture(%1s)</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠤᠯᠵᠠᠯᠠᠬᠤ(%1s)</translation>
    </message>
    <message>
        <source>Get code</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠤᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KBTitle</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="46"/>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="83"/>
        <source>Suspended state</source>
        <translation>ᠳᠡᠭᠦᠵᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="57"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/kbtitle.cpp" line="86"/>
        <source>Welt status</source>
        <translation>ᠨᠠᠭᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <source>KeyboardWidget</source>
        <translation type="vanished">ᠳᠠᠷᠤᠭᠤᠯ᠎ᠤᠨ ᠵᠢᠵᠢᠭ ᠲᠤᠨᠤᠭᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">麒麟网络工具信息提示</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <source>kylin-nm</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <source>LAN</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Enabel LAN List</source>
        <translation type="obsolete">其他有线网络</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation type="obsolete">无线网络</translation>
    </message>
    <message>
        <source>Enabel WiFi List</source>
        <translation type="obsolete">其他无线网络</translation>
    </message>
    <message>
        <source>New WiFi</source>
        <translation type="obsolete">加入其他网络</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">已开启</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已关闭</translation>
    </message>
    <message>
        <source>HotSpot</source>
        <translation type="vanished">ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠬᠠᠯᠠᠮᠱᠢᠯ ᠴᠡᠭ</translation>
    </message>
    <message>
        <source>FlyMode</source>
        <translation type="vanished">ᠨᠢᠰᠦᠯᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="vanished">显示网络连接界面</translation>
    </message>
    <message>
        <source>Inactivated LAN</source>
        <translation type="vanished">ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Inactivated WLAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <source>Other WLAN</source>
        <translation type="vanished">ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ᠎ᠦ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Show KylinNM</source>
        <translation type="vanished">KylinNM᠎ᠶᠢ/᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>No wireless card detected</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ᠎ᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Activated LAN</source>
        <translation type="vanished">ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <source>Activated WLAN</source>
        <translation type="vanished">ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="vanished">ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>No Other Wired Network Scheme</source>
        <translation type="vanished">ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠤᠰᠤᠳ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>No wifi connected.</source>
        <translation type="obsolete">未连接任何网络</translation>
    </message>
    <message>
        <source>No Other Wireless Network Scheme</source>
        <translation type="vanished">ᠪᠤᠰᠤᠳ ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Wired net is disconnected</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="obsolete">断开无线网络</translation>
    </message>
    <message>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation type="vanished">Wi-Fi᠎ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠶᠤ ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠪᠠᠳᠤᠯᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">其他有线网络</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">新建有线网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>No usable network in the list</source>
        <translation type="vanished">ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ᠎ᠳᠤ ᠤᠳᠤᠬᠠᠨ᠎ᠳᠠᠭᠠᠨ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>NetOn,</source>
        <translation type="vanished">ᠨᠢᠭᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ ᠂</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">其他无线网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">打开无线网开关前保持有线网开关打开</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">请先插入无线网卡</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">正在断开异常连接的网络</translation>
    </message>
    <message>
        <source>update Wi-Fi list now, click again</source>
        <translation type="vanished">正在更新 Wi-Fi列表 请再次点击</translation>
    </message>
    <message>
        <source>update Wi-Fi list now</source>
        <translation type="vanished">正在更新 Wi-Fi列表</translation>
    </message>
    <message>
        <source>Conn Ethernet Success</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠠᠮᠵᠢᠯᠳᠠᠳᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Conn Ethernet Fail</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Conn Wifi Success</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠠᠮᠵᠢᠯᠳᠠᠳᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="172"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/letterswidget.cpp" line="186"/>
        <source>123</source>
        <translation>123</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation type="vanished">ᠳᠠᠷᠤᠶ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation type="vanished">ᠣᠷᠣᠯᠠᠭᠤᠯᠬᠤ ᠵᠦᠢᠯ ᠢ ᠣᠷᠣᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LightDMHelper</name>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="121"/>
        <source>failed to start session.</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠯᠭ᠎ᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="215"/>
        <source>Login</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/lightdmhelper.cpp" line="224"/>
        <source>Guest</source>
        <translation>ᠵᠤᠷᠴᠢᠭᠴᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <source>Date</source>
        <translation type="vanished">ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="581"/>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="568"/>
        <source>VirtualKeyboard</source>
        <translation>ᠬᠡᠶᠢᠰᠬᠡᠷ ᠲᠠᠷᠤᠭᠤᠯ ᠤᠨ ᠫᠠᠨᠰᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="483"/>
        <source>SwitchSession</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="501"/>
        <source>Power Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="514"/>
        <source>SwitchUser</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠰᠣᠯᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="544"/>
        <source>Network</source>
        <translation type="unfinished">网络</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>LAN</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ᠎ᠦ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="625"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1727"/>
        <source>system-monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1253"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1259"/>
        <source>%1 may cause users who have logged in to this computer to lose content that has not yet been stored,To still perform please click &quot;Confirm&quot;.</source>
        <translation>%1 ᠨᠢ ᠡᠨᠡ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠲᠦ ᠨᠢᠭᠡᠨᠲᠡ ᠭᠠᠷᠴᠤ ᠮᠡᠳᠡᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠳᠤᠶ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠨ ᠭᠡᠭᠡᠵᠦ ᠮᠡᠳᠡᠨ᠎ᠡ ᠂ ᠮᠥᠨ ᠬᠦ 《 ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ 》 ᠶᠢ ᠮᠥᠨ ᠬᠦ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1255"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1290"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1289"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1294"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to %1 this system?</source>
        <translation>ᠣᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠬᠠᠮᠲᠤ ᠳᠤᠨᠢ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠵᠡᠢ ᠃ ᠲᠠ %1 ᠡᠨᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>PowerOff</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/lockwidget.cpp" line="1261"/>
        <location filename="../src/widgets/lockwidget.cpp" line="1295"/>
        <source>Shut Down</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="82"/>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="105"/>
        <source>Login Options</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌ ᠰᠤᠩᠭᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="289"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="315"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1034"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1036"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1038"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1040"/>
        <source>Face</source>
        <translation>ᠴᠢᠷᠠᠢ᠎ᠪᠠᠷ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1042"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1044"/>
        <source>Ukey</source>
        <translation>ᠠᠶᠤᠯᠭᠦᠢ ᠨᠢᠭᠤᠴᠠ ᠲᠦᠯᠬᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="1046"/>
        <source>QRCode</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../src/widgets/loginoptionswidget.cpp" line="703"/>
        <source>Identify device removed!</source>
        <translation>ᠬᠠᠷᠭᠤᠭᠤᠯᠵᠤ ᠱᠢᠯᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠱᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>MPRISWidget</name>
    <message>
        <location filename="../src/widgets/mpriswidget.cpp" line="214"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="215"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="228"/>
        <location filename="../src/widgets/mpriswidget.cpp" line="237"/>
        <source>Unknown</source>
        <translation>ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <source>Verification code</source>
        <translation type="vanished">ᠤᠬᠤᠷ ᠮᠡᠳᠡᠭᠡᠨ᠎ᠦ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋</translation>
    </message>
</context>
<context>
    <name>MyNetworkWidget</name>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="52"/>
        <source>LAN</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/mynetworkwidget.cpp" line="54"/>
        <source>WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ᠎ᠦ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="161"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="175"/>
        <location filename="../src/VirtualKeyboard/src/numberswidget.cpp" line="310"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <source>Automatically join the network</source>
        <translation type="vanished">ᠲᠤᠰ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠠᠦ᠋ᠲ᠋ᠤ᠋᠎ᠪᠠᠷ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Input Password...</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ...</translation>
    </message>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Signal：</source>
        <translation type="vanished">ᠲᠤᠬᠢᠶᠠᠨ᠎ᠤ ᠡᠷᠴᠢᠮ ᠄</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">开放</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">安全</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">速率</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>WiFi Security：</source>
        <translation type="vanished">Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ：</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="vanished">ᠹᠢᠼᠢᠺ᠎ᠦᠨ ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <source>Conn Wifi Failed</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">ᠲᠠᠰᠤᠷᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>No Configuration</source>
        <translation type="vanished">ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>IPv4：</source>
        <translation type="vanished">IPv4 ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <source>IPv6：</source>
        <translation type="vanished">IPv6 ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <source>BandWidth：</source>
        <translation type="vanished">ᠪᠦᠰᠡ᠎ᠶᠢᠨ ᠦᠷᠭᠡᠨ ᠄</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="vanished">MAC ᠬᠠᠶᠢᠭ：</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">自动</translation>
    </message>
</context>
<context>
    <name>PhoneAuthWidget</name>
    <message>
        <source>Verification by phoneNum</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>「 Use bound Phone number to verification 」</source>
        <translation type="vanished">「ᠤᠶᠠᠭᠰᠠᠨ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠭᠠᠷᠠᠢ」</translation>
    </message>
    <message>
        <source>「 Use SMS to verification 」</source>
        <translation type="vanished">「ᠲᠤᠰ ᠳᠠᠩᠰᠠᠨ᠎ᠳᠤ ᠤᠶᠠᠭᠰᠠᠨ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠭᠠᠷᠠᠢ」</translation>
    </message>
    <message>
        <source>commit</source>
        <translation type="vanished">ᠳᠤᠱᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Network not connected~</source>
        <translation type="vanished">ᠱᠢᠰᠲ᠋ᠧᠮ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠬᠤᠯᠪᠤᠭᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <source>Network unavailable~</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠢᠳᠠᠯ ᠮᠠᠭᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <source>Verification Code invalid!</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <source>Verification Code incorrect.Please retry!</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ! ᠵᠦᠪ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠭᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Failed time over limit!Retry after 1 hour!</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ10᠎ᠶᠢ ᠬᠡᠳᠦᠷᠡᠪᠡ ᠂1 ᠴᠠᠭ᠎ᠤᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>verifaction failed!</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>PowerListWidget</name>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="299"/>
        <location filename="../src/widgets/powerlistwidget.h" line="109"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="300"/>
        <location filename="../src/widgets/powerlistwidget.h" line="110"/>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠴᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠃ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="303"/>
        <location filename="../src/widgets/powerlistwidget.h" line="122"/>
        <source>Suspend</source>
        <translation>ᠵᠤᠭ᠍ᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="vanished">ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠮᠠᠰᠢᠨ ᠢᠶᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠂ ᠭᠡᠪᠡᠴᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠣᠷᠣᠭᠳᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠪᠠᠭ᠎ᠠ ᠃ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠭᠡᠷ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠲᠦᠷᠭᠡᠨ ᠰᠡᠷᠢᠭᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="307"/>
        <location filename="../src/widgets/powerlistwidget.h" line="172"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="308"/>
        <source>Close all apps, and then restart your computer</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="310"/>
        <location filename="../src/widgets/powerlistwidget.h" line="196"/>
        <source>Shut Down</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="315"/>
        <location filename="../src/widgets/powerlistwidget.h" line="149"/>
        <source>The current user logs out of the system, terminates the session, and returns to the login page</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠡᠴᠡ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠵᠤ ᠂ ᠲᠡᠭᠦᠨ ᠦ ᠬᠤᠷᠠᠯ ᠤᠨ ᠦᠭᠡ ᠪᠡᠨ ᠳᠠᠭᠤᠰᠬᠠᠬᠤ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠡᠮᠳᠡᠭᠯᠡᠭᠰᠡᠨ ᠨᠢᠭᠤᠷ ᠲᠤ ᠪᠤᠴᠠᠵᠠᠶ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="325"/>
        <source>Logout</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="311"/>
        <location filename="../src/widgets/powerlistwidget.h" line="197"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="317"/>
        <location filename="../src/widgets/powerlistwidget.h" line="97"/>
        <source>SwitchUser</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠰᠣᠯᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="319"/>
        <location filename="../src/widgets/powerlistwidget.h" line="135"/>
        <source>LockScreen</source>
        <translation>ᠳᠡᠯᠪᠡᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="313"/>
        <location filename="../src/widgets/powerlistwidget.h" line="147"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="304"/>
        <location filename="../src/widgets/powerlistwidget.h" line="123"/>
        <source>The computer stays on, but consumes less power, The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠮᠠᠰᠢᠨ ᠢᠶᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠂ ᠭᠡᠪᠡᠴᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠣᠷᠣᠭᠳᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠪᠠᠭ᠎ᠠ ᠃ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠭᠡᠷ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠲᠦᠷᠭᠡᠨ ᠰᠡᠷᠢᠭᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="321"/>
        <location filename="../src/widgets/powerlistwidget.h" line="160"/>
        <source>UpgradeThenRestart</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠰᠡᠩᠬᠡᠷᠡᠭᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.h" line="173"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/widgets/powerlistwidget.cpp" line="323"/>
        <location filename="../src/widgets/powerlistwidget.h" line="184"/>
        <source>UpgradeThenShutdown</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠪᠠᠢ᠌ᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠨ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <source>lock</source>
        <translation type="vanished">ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation type="vanished">ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</source>
        <translation type="vanished">ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Turn off your computer, but the app stays open, When the computer is turned on, it can be restored to the state you left</source>
        <translation type="vanished">ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠴᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠃ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation type="vanished">ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠮᠠᠰᠢᠨ ᠢᠶᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠂ ᠭᠡᠪᠡᠴᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠣᠷᠣᠭᠳᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠪᠠᠭ᠎ᠠ ᠃ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠭᠡᠷ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠲᠦᠷᠭᠡᠨ ᠰᠡᠷᠢᠭᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="108"/>
        <source>The screensaver is active.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠠᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="110"/>
        <source>The screensaver is inactive.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠠᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="28"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="30"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="32"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="34"/>
        <source>Face</source>
        <translation>ᠴᠢᠷᠠᠢ᠎ᠪᠠᠷ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="36"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="38"/>
        <source>Ukey</source>
        <translation>ᠠᠶᠤᠯᠭᠦᠢ ᠨᠢᠭᠤᠴᠠ ᠲᠦᠯᠬᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/common/biodefines.cpp" line="40"/>
        <source>QRCode</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="../src/dbusifs/enginedevice.cpp" line="308"/>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">退出(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="136"/>
        <source>Picture does not exist</source>
        <translation>ᠵᠢᠷᠤᠭ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="vanished">设置为桌面壁纸</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="vanished">自动切换</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="obsolete">您有%1条未读消息</translation>
    </message>
    <message>
        <source>You have new notification</source>
        <translation type="vanished">ᠲᠠ ᠱᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤᠯᠤᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/screensaver/screensaver.cpp" line="139"/>
        <source>View</source>
        <translation>ᠬᠡᠪ ᠦᠵᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <source>You have rested:</source>
        <translation type="vanished">ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠠᠮᠠᠷᠠᠪᠠ ᠄</translation>
    </message>
    <message>
        <location filename="../src/screensaver/sleeptime.cpp" line="75"/>
        <source>You have rested</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠮᠠᠷᠠᠵᠠᠢ</translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠱᠤᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠤᠨᠳᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠬᠠᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>login by password</source>
        <translation type="vanished">密码登录</translation>
    </message>
    <message>
        <source>login by qr code</source>
        <translation type="vanished">微信登录</translation>
    </message>
</context>
<context>
    <name>SwitchButtonGroup</name>
    <message>
        <source>uEduPWD</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>TabletLockWidget</name>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>New password is the same as old</source>
        <translation type="vanished">ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠤᠤᠯ᠎ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠠᠳᠠᠯᠢ</translation>
    </message>
    <message>
        <source>Reset password error:%1</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠄%1</translation>
    </message>
    <message>
        <source>Please scan by correct WeChat</source>
        <translation type="vanished">ᠲᠠ ᠵᠦᠪ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation type="vanished">ᠦᠰᠦᠷᠴᠤ᠌ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UserListWidget</name>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="67"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="121"/>
        <source>Login</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/widgets/userlistwidget.cpp" line="69"/>
        <location filename="../src/widgets/userlistwidget.cpp" line="123"/>
        <source>Guest</source>
        <translation>ᠵᠤᠷᠴᠢᠭᠴᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="vanished">ᠴᠢ ᠯᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠭᠠᠵᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ᠎ᠪᠠᠷ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>VerificationWidget</name>
    <message>
        <source>Please scan by bound WeChat</source>
        <translation type="vanished">ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>VerticalVerificationWidget</name>
    <message>
        <source>Please scan by bound WeChat</source>
        <translation type="vanished">ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>WeChatAuthDialog</name>
    <message>
        <source>Login by wechat</source>
        <translation type="vanished">ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Verification by wechat</source>
        <translation type="vanished">ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>「 Use registered WeChat account to login 」</source>
        <translation type="vanished">「 ᠨᠢᠭᠡᠨᠳᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠸᠢᠴᠠᠲ᠎ᠤᠨ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌」</translation>
    </message>
    <message>
        <source>「 Use bound WeChat account to verification 」</source>
        <translation type="vanished">「 ᠲᠤᠰ ᠳᠠᠩᠰᠠᠨ᠎ᠳᠤ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ」</translation>
    </message>
    <message>
        <source>Network not connected~</source>
        <translation type="vanished">ᠱᠢᠰᠲ᠋ᠧᠮ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠬᠤᠯᠪᠤᠭᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <source>Scan code successfully</source>
        <translation type="vanished">ᠺᠤᠳ᠋ ᠱᠢᠷᠪᠢᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Timeout!Try again!</source>
        <translation type="vanished">ᠴᠠᠭ ᠬᠡᠳᠦᠷᠡᠪᠡ! ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation type="vanished">登录失败</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>ᠵᠢᠵᠢᠭ ᠲᠤᠨᠤᠭᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../examples/LoadCustomPlugin/widget.ui" line="26"/>
        <source>LoadPlugin</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠲᠣᠨᠣᠭ ᠨᠡᠮᠡᠵᠦ ᠠᠴᠢᠶᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>action</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="76"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="81"/>
        <source>which block type</source>
        <translation>ᠶᠠᠮᠠᠷ ᠬᠡᠰᠡᠭ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠤᠢ ?</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="69"/>
        <source>which block type,param:Suspend/Hibernate/Restart/Shutdown/Logout</source>
        <translation>ᠶᠠᠮᠠᠷ ᠬᠡᠰᠡᠭ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠂ ᠪᠤᠯᠵᠣᠯᠲᠤ ᠲᠣᠭ᠎ᠠ ᠄ ᠨᠣᠶᠢᠷ / ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ / ᠪᠣᠭᠣᠮᠲᠠ / ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="74"/>
        <source>which block type,param:Shutdown/Restart</source>
        <translation>ᠶᠠᠮᠠᠷ ᠬᠡᠰᠡᠭ ᠬᠡᠯᠪᠡᠷᠢ ᠂ param ᠄ ᠬᠠᠭᠠᠯᠭ᠎ᠠ / ᠳᠠᠬᠢᠨ ᠰᠡᠩᠬᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="62"/>
        <source>how long to show lock</source>
        <translation>ᠡᠳᠦᠢ ᠤᠷᠲᠤ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ᠎ᠪᠠᠷ ᠣᠨᠢᠰᠣ᠎ᠶ᠋ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="67"/>
        <source>if show lock</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠪᠡᠯ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/lock-command/main.cpp" line="44"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠵᠠᠷᠯᠢᠭ᠎ᠲᠤ ᠬᠦᠷᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="49"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="46"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="48"/>
        <source>lock the screen immediately</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳᠡᠯᠬᠡᠴᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="52"/>
        <source>query the status of the screen saver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠢ ᠤᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="55"/>
        <source>unlock the screen saver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="57"/>
        <source>show the screensaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="60"/>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠬᠤ ᠪᠥᠭᠡᠳ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠬᠣᠢᠰᠢᠯᠠᠭᠤᠯᠬᠤ ᠫᠷᠠᠮᠧᠲ᠋ᠷ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠄ ᠰᠤᠯᠠ / ᠳᠡᠭᠡᠷ᠎ᠡ ᠬᠠᠪᠬᠠᠭᠠᠰᠤ / ᠪᠠᠭ᠎ᠠ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="63"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="70"/>
        <source>show the session tools</source>
        <translation>ᠬᠤᠷᠠᠯ ᠤᠨ ᠪᠠᠭᠠᠵᠢ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="65"/>
        <source>show the switchuser window</source>
        <translation>ᠰᠧᠺᠲ᠋ᠠ ᠶᠢᠨ ᠴᠣᠩᠬᠣ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="68"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="75"/>
        <source>show the app block window</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠴᠣᠩᠬᠣ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠶᠢ ᠬᠠᠷᠰᠢᠯᠠᠭᠰᠠᠨ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/lock-command/main.cpp" line="73"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="80"/>
        <source>show the multiUsers block window</source>
        <translation>multiUsrs ᠬᠡᠰᠡᠭ ᠴᠣᠩᠬᠣ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="40"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠦᠨ ᠶᠠᠷᠢᠯᠴᠠᠯᠭ᠎ᠠ᠎ᠶᠢᠨ ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="50"/>
        <source>activated by session idle signal</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠰᠤᠯᠠ ᠳᠣᠬᠢᠶ᠎ᠠ᠎ᠪᠠᠷ ᠢᠳᠡᠪᠬᠢᠵᠢᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="53"/>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="58"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="55"/>
        <source>show screensaver immediately</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠡᠮ᠎ᠢ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="61"/>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠦ ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠴᠠᠭ᠎ᠢ᠋ ᠬᠣᠢᠰᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="66"/>
        <source>show blank screensaver immediately and if lock</source>
        <translation>ᠳᠠᠷᠤᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠂ ᠬᠡᠷᠪᠡ ᠣᠨᠢᠰᠤᠯᠠᠪᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/lock-dialog/lockdialogmodel.cpp" line="72"/>
        <source>show switch user window</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠴᠣᠩᠬᠣ ᠶᠢ ᠰᠣᠯᠢᠬᠤ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="63"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation>ukui—screensaver ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="67"/>
        <source>show on root window</source>
        <translation>ᠦᠨᠳᠦᠰᠦ ᠴᠣᠩᠬᠣᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="69"/>
        <source>show on window.</source>
        <translation>ᠴᠣᠩᠬᠣᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/screensaver/main.cpp" line="70"/>
        <source>window id</source>
        <translation>ᠴᠣᠩᠬᠣᠨ᠎ᠤ᠋ id</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="56"/>
        <source>Backend for the ukui ScreenSaver.</source>
        <translation>ukuui Screenar ᠶᠢᠨ ᠠᠷᠤ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/lock-backend/main.cpp" line="62"/>
        <source>lock the screen by startup</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
</context>
</TS>
